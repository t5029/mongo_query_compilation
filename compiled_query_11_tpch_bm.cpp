#include <benchmark/benchmark.h>
#include <boost/regex.hpp>
#include <cstring>
#include <functional>
#include <limits>
#include <memory>
#include <string.h>
#include <unordered_map>
#include <vector>

#include "mongo/db/catalog/query_compilation_benchmarks/benchmark_utils.hpp"

namespace mongo {
namespace tpch {
namespace {
void BM_Q11_TPCH(benchmark::State& state) {
    SETUP();
    Collection* supplier =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "supplier"));
    Collection* part =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "part"));

    for (auto _ : state) {
        struct Aggs {
            double value;
        };
        struct Supplier {
            std::vector<int> s_parts;
        };
        auto supplierCursor = supplier->getCursor(opCtx.get());
        auto partCursor = part->getCursor(opCtx.get());

        const std::string& param1 = "ARGENTINA";

        std::unordered_map<int, Supplier> suppliers;
        while (auto record = supplierCursor->next()) {
            auto bson = record.get().data.toBson();
            auto s_suppkey = bson["_id"].Int();
            auto s_nation = bson["s_nation"].Obj();
            auto n_name = s_nation["n_name"].String();
            if (n_name == param1) {
                auto s_parts = bson["s_parts"].Array();
                std::vector<int> casted_parts(s_parts.size());
                std::transform(s_parts.begin(), s_parts.end(), casted_parts.begin(), [](auto p) {
                    return p.Int();
                });
                suppliers.insert(std::make_pair(s_suppkey, Supplier{casted_parts}));
            }
        }

        double uncorrelated_sum = 0;
        std::unordered_map<int, Aggs> groups;
        while (auto record = partCursor->next()) {
            auto bson = record.get().data.toBson();
            auto p_partkey = bson["_id"].Int();
            auto p_suppliers = bson["p_suppliers"].Array();
            double value = 0;
            for (const auto& s : p_suppliers) {
                auto ps_suppkey = s["ps_suppkey"].Int();
                auto ps_supplycost = s["ps_supplycost"].Number();
                auto ps_availqty = s["ps_availqty"].Number();
                if (auto found = suppliers.find(ps_suppkey); found != suppliers.end()) {
                    auto cost = ps_supplycost * ps_availqty;
                    uncorrelated_sum += cost;
                    value += cost;
                }
            }
            groups[p_partkey] = Aggs{value};
        }
        uncorrelated_sum *= 0.0001000000;

        auto it = groups.begin();
        while (it != groups.end()) {
            if (it->second.value <= uncorrelated_sum) {
                it = groups.erase(it);
            } else {
                ++it;
            }
        }

        benchmark::DoNotOptimize(groups);
        std::vector<int> sorted_groups(groups.size());
        std::transform(groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });
        std::sort(sorted_groups.begin(), sorted_groups.end(), [&groups](int a, int b) {
            auto value_a = groups[a].value;
            auto value_b = groups[b].value;
            return value_a > value_b;
        });
        benchmark::DoNotOptimize(sorted_groups);

        // for (auto i = sorted_groups.begin(); i < sorted_groups.end(); ++i) {
        //     printf("\nps_partkey: %d, value: %f", *i, groups[*i].value);
        // }

        supplierCursor.reset();
        partCursor.reset();
    }
    CLEANUP();
}

BENCHMARK(BM_Q11_TPCH)->Unit(benchmark::kMillisecond);

}  // namespace
}  // namespace tpch
}  // namespace mongo