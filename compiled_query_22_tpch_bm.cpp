#include <algorithm>
#include <benchmark/benchmark.h>
#include <memory>
#include <unordered_map>
#include <vector>

#include "mongo/db/catalog/query_compilation_benchmarks/benchmark_utils.hpp"

namespace mongo {
namespace tpch {
namespace {

void BM_Q22_S2_TPCH_PARTITIONED(benchmark::State& state) {
    SETUP();

    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "customer"));
    Collection* orders =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "orders"));

    for (auto _ : state) {
        using group_key = std::string;
        struct Aggs {
            int numcust;
            double totacctbal;
            std::vector<double> c_acctbals;
        };

        auto customerCursor = customer->getCursor(opCtx.get());
        auto ordersCursor = orders->getCursor(opCtx.get());

        const std::vector<std::string> param1{"30", "17", "25", "10", "22", "15", "21"};

        std::unordered_set<int> customers_with_orders;
        while (auto record = ordersCursor->next()) {
            auto bson = record.get().data.toBson();
            auto o_custkey = bson["o_custkey"].Int();
            customers_with_orders.insert(o_custkey);
        }

        using vector_type = std::pair<std::string, double>;
        auto v = std::vector<vector_type>();

        double sum_c_acctbal = 0;
        double count_c_acctbal = 0;
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            auto c_acctbal = bson["c_acctbal"].Number();

            if (c_acctbal <= 0)
                continue;

            auto c_custkey = bson["_id"].Int();
            auto c_phone = bson["c_phone"].String();
            auto cntrycode = c_phone.substr(0, 2);
            auto in_param1 = (std::find(param1.begin(), param1.end(), cntrycode) != param1.end());

            if (in_param1) {
                auto customer_without_orders =
                    (customers_with_orders.find(c_custkey) == customers_with_orders.end());
                if (customer_without_orders) {
                    v.emplace_back(make_pair(cntrycode, c_acctbal));
                }
                count_c_acctbal++;
                sum_c_acctbal += c_acctbal;
            }
        }
        benchmark::DoNotOptimize(v);
        const double avg_acctbal = sum_c_acctbal / count_c_acctbal;

        // c_acctbal > avg(c_acctbal)
        auto filtered = std::partition(v.begin(), v.end(), [&avg_acctbal](vector_type& v) {
            return (v.second > avg_acctbal);
        });

        // sort v
        std::sort(v.begin(), filtered, [](auto& p1, auto& p2) { return p1.first < p2.first; });

        auto cntycode_prev = v.cbegin()->first;
        int numcust = 0;
        double totalbalance = 0;

        for (auto it = v.begin(); it != filtered; ++it) {
            auto cntrycode = it->first;
            if (cntrycode == cntycode_prev) {
                numcust++;
                totalbalance += it->second;
            } else {
                // printf("cntrycode: %s, numcust: %d, totacctbal: %f",
                //        cntycode_prev.c_str(),
                //        numcust,
                //        totalbalance);
                cntycode_prev = cntrycode;
                numcust = 1;
                totalbalance = it->second;
            }
        }
        // // the last one
        // printf("cntrycode: %s, numcust: %d, totacctbal: %f",
        //        cntycode_prev.c_str(),
        //        numcust,
        //        totalbalance);

        customerCursor.reset();
        ordersCursor.reset();
    }
    CLEANUP();
}

void BM_Q22_S2_TPCH(benchmark::State& state) {
    SETUP();

    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "customer"));
    Collection* orders =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "orders"));

    for (auto _ : state) {
        using group_key = std::string;
        struct Aggs {
            int numcust;
            double totacctbal;
            std::vector<double> c_acctbals;
        };

        auto customerCursor = customer->getCursor(opCtx.get());
        auto ordersCursor = orders->getCursor(opCtx.get());

        const std::vector<std::string> param1{"30", "17", "25", "10", "22", "15", "21"};

        // Use hashtable for anti-join. Discard tuple on
        // successful probe.
        std::unordered_set<int> customers_with_orders;
        while (auto record = ordersCursor->next()) {
            auto bson = record.get().data.toBson();
            auto o_custkey = bson["o_custkey"].Int();
            customers_with_orders.insert(o_custkey);
        }

        auto groups = std::map<std::string, Aggs>();
        double sum_c_acctbal = 0;
        double count_c_acctbal = 0;
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            auto c_custkey = bson["_id"].Int();
            auto c_acctbal = bson["c_acctbal"].Number();
            auto c_phone = bson["c_phone"].String();
            auto cntrycode = c_phone.substr(0, 2);

            // SQL Equivalent:
            // substring(c_phone from 1 for 2) in
            //	 ('30', '17', '25', '10', '22', '15', '21')
            auto in_param1 = (std::find(param1.begin(), param1.end(), cntrycode) != param1.end());
            // SQL Equivalent:
            // not exists ( select * from orders where o_custkey = c_custkey )
            auto customer_without_orders =
                (customers_with_orders.find(c_custkey) == customers_with_orders.end());

            // OPTIMIZATION PATTERN:
            // Coalesce outer and subquery over the same table
            // * DESCRIPTION:
            //   * Push-down selections unrelated to the table in the outer query
            //   * Pull-up (or leave as is) selection involving subquery, keeping track of
            //   "c_acctbal"s
            //     to substract from numcust and totalaccountbal later.
            //   * Compute "avg(c_acctbal) where c_acctbal>0" in first pass, but do selection after
            //   grouping.
            //     Because c_acctbal > 0 in subquery, we know c_acctbal > avg(c_acctbal) > 0 in
            //     outer query.
            // * TRIGGERED BY:
            //   * Selection involving scalar subquery
            if (in_param1 && c_acctbal > 0) {
                if (customer_without_orders) {
                    Aggs& aggs = groups[cntrycode];
                    aggs.numcust++;
                    aggs.totacctbal += c_acctbal;
                    aggs.c_acctbals.emplace_back(c_acctbal);
                }
                count_c_acctbal++;
                sum_c_acctbal += c_acctbal;
            }
        }
        benchmark::DoNotOptimize(groups);
        const double avg_acctbal = sum_c_acctbal / count_c_acctbal;

        for (const auto& it : groups) {
            auto cntrycode = it.first;
            Aggs& aggs = groups[cntrycode];
            auto& c_acctbals = aggs.c_acctbals;
            std::for_each(
                c_acctbals.begin(), c_acctbals.end(), [&aggs, &avg_acctbal](double& c_acctbal) {
                    if (c_acctbal <= avg_acctbal) {
                        aggs.numcust--;
                        aggs.totacctbal -= c_acctbal;
                    }
                });
            // printf("cntrycode: %s, numcust: %d, totacctbal: %f",
            //        cntrycode.c_str(),
            //        aggs.numcust,
            //        aggs.totacctbal);
        }

        customerCursor.reset();
        ordersCursor.reset();
    }
    CLEANUP();
}

void BM_Q22_S1_TPCH(benchmark::State& state) {
    SETUP();

    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "scale1"));

    for (auto _ : state) {
        using group_key = std::string;
        struct Aggs {
            int numcust;
            double totacctbal;
            std::vector<double> c_acctbals;
        };

        auto customerCursor = customer->getCursor(opCtx.get());

        const std::vector<std::string> param1{"30", "17", "25", "10", "22", "15", "21"};

        auto groups = std::map<std::string, Aggs>();
        double sum_c_acctbal = 0;
        double count_c_acctbal = 0;
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            auto c_acctbal = bson["c_acctbal"].Number();
            auto c_phone = bson["c_phone"].String();
            auto cntrycode = c_phone.substr(0, 2);

            // SQL Equivalent:
            // `substring(c_phone from 1 for 2) in
            //	 ('30', '17', '25', '10', '22', '15', '21')`
            auto in_param1 = (std::find(param1.begin(), param1.end(), cntrycode) != param1.end());
            // SQL Equivalent:
            // `not exists ( select * from orders where o_custkey = c_custkey )`
            // Note that this could also be represented as `!bson.hasField("c_orders")` if the
            // schema had documents without `c_orders`.
            auto customer_without_orders = (bson["c_orders"].Array().empty());

            if (in_param1 && c_acctbal > 0) {
                if (customer_without_orders) {
                    Aggs& aggs = groups[cntrycode];
                    aggs.numcust++;
                    aggs.totacctbal += c_acctbal;
                    aggs.c_acctbals.emplace_back(c_acctbal);
                }
                count_c_acctbal++;
                sum_c_acctbal += c_acctbal;
            }
        }
        benchmark::DoNotOptimize(groups);
        const double avg_acctbal = sum_c_acctbal / count_c_acctbal;

        for (const auto& it : groups) {
            auto cntrycode = it.first;
            Aggs& aggs = groups[cntrycode];
            auto& c_acctbals = aggs.c_acctbals;
            std::for_each(
                c_acctbals.begin(), c_acctbals.end(), [&aggs, &avg_acctbal](double& c_acctbal) {
                    if (c_acctbal <= avg_acctbal) {
                        aggs.numcust--;
                        aggs.totacctbal -= c_acctbal;
                    }
                });
            // printf("cntrycode: %s, numcust: %d, totacctbal: %f",
            //        cntrycode.c_str(),
            //        aggs.numcust,
            //        aggs.totacctbal);
        }

        customerCursor.reset();
    }
    CLEANUP();
}

BENCHMARK(BM_Q22_S2_TPCH_PARTITIONED)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_Q22_S2_TPCH)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_Q22_S1_TPCH)->Unit(benchmark::kMillisecond);

}  // namespace
}  // namespace tpch
}  // namespace mongo
