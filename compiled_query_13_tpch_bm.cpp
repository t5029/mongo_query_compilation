#include <benchmark/benchmark.h>
#include <memory>
#include <regex>
#include <unordered_map>
#include <vector>

#include "mongo/db/catalog/query_compilation_benchmarks/benchmark_utils.hpp"

namespace mongo {
namespace tpch {
namespace {
void BM_Q13_S2_TPCH(benchmark::State& state) {
    SETUP();

    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "customer"));
    Collection* orders =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "orders"));

    for (auto _ : state) {
        struct Aggs1 {
            int c_count;
        };
        struct Aggs2 {
            int custdist;
        };
        auto customerCursor = customer->getCursor(opCtx.get());
        auto ordersCursor = orders->getCursor(opCtx.get());

        auto param1 = std::regex("(.*)(express)(.*)(packages)(.*)");

        auto groups1 = std::unordered_map<int, Aggs1>();
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            auto c_custkey = bson["_id"].Int();
            groups1.insert(std::make_pair(c_custkey, Aggs1{0}));
        }

        while (auto record = ordersCursor->next()) {
            auto bson = record.get().data.toBson();
            // stats would tell us that all orders have the field
            // o_custkey, but the system doesn't know it's a
            // foreign key. Thus, we still need to check if it's
            // on the hashtable.
            auto o_custkey = bson["o_custkey"].Int();
            auto o_comment = bson["o_comment"].String();
            auto found = groups1.find(o_custkey);
            if (!std::regex_match(o_comment, param1) && found != groups1.end()) {
                Aggs1& aggs = groups1[o_custkey];
                aggs.c_count++;
            }
        }
        benchmark::DoNotOptimize(groups1);

        auto groups2 = std::unordered_map<int, Aggs2>();
        for (const auto& group : groups1) {
            auto c_count = group.second.c_count;
            Aggs2& agg = groups2[c_count];
            agg.custdist++;
        }
        benchmark::DoNotOptimize(groups2);

        std::vector<std::pair<int, int>> sorted_groups(groups2.size());
        std::transform(groups2.begin(), groups2.end(), sorted_groups.begin(), [](auto pair) {
            return std::make_pair(pair.first, pair.second.custdist);
        });
        std::sort(sorted_groups.begin(),
                  sorted_groups.end(),
                  [](std::pair<int, int>& a, std::pair<int, int>& b) {
                      return a.second > b.second || (a.second == b.second && a.first < b.first);
                  });
        benchmark::DoNotOptimize(sorted_groups);

        // for (const auto& [c_count, custdist] : sorted_groups) {
        //     printf("\nc_count: %d, custdist:%d", c_count, custdist);
        // }

        customerCursor.reset();
        ordersCursor.reset();
    }
    CLEANUP();
}

void BM_Q13_S1_TPCH(benchmark::State& state) {
    SETUP();

    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "scale1"));

    for (auto _ : state) {
        struct Aggs1 {
            int c_count;
        };
        struct Aggs2 {
            int custdist;
        };
        auto customerCursor = customer->getCursor(opCtx.get());

        auto param1 = std::regex("(.*)(express)(.*)(packages)(.*)");

        auto groups1 = std::unordered_map<int, Aggs1>();
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            auto c_custkey = bson["_id"].Int();
            Aggs1 aggs = Aggs1{0};
            if (bson.hasField("c_orders")) {
                auto orders = bson["c_orders"].Array();
                for (const auto& order : orders) {
                    auto o_comment = order["o_comment"].String();
                    if (!std::regex_match(o_comment, param1)) {
                        aggs.c_count++;
                    }
                }
            }
            groups1.insert(std::make_pair(c_custkey, aggs));
        }
        benchmark::DoNotOptimize(groups1);

        auto groups2 = std::unordered_map<int, Aggs2>();
        for (const auto& group : groups1) {
            auto c_count = group.second.c_count;
            Aggs2& agg = groups2[c_count];
            agg.custdist++;
        }
        benchmark::DoNotOptimize(groups2);

        std::vector<std::pair<int, int>> sorted_groups(groups2.size());
        std::transform(groups2.begin(), groups2.end(), sorted_groups.begin(), [](auto pair) {
            return std::make_pair(pair.first, pair.second.custdist);
        });
        std::sort(sorted_groups.begin(),
                  sorted_groups.end(),
                  [](std::pair<int, int>& a, std::pair<int, int>& b) {
                      return a.second > b.second || (a.second == b.second && a.first < b.first);
                  });
        benchmark::DoNotOptimize(sorted_groups);

        // for (const auto& [c_count, custdist] : sorted_groups) {
        //     printf("\nc_count: %d, custdist:%d", c_count, custdist);
        // }

        customerCursor.reset();
    }
    CLEANUP();
}

BENCHMARK(BM_Q13_S2_TPCH)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_Q13_S1_TPCH)->Unit(benchmark::kMillisecond);

}  // namespace
}  // namespace tpch
}  // namespace mongo
