conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.lineitem.explain("allPlansExecution").aggregate([
  {$match: { $expr:{
    $and:[
      {$gte:["$l_shipdate",  new Date(ISODate("1992-04-30T00:00:00Z"))]},
      {$lt:["$l_shipdate",  new Date(ISODate("1993-04-30T00:00:00Z"))]},
      {$gte:["$l_discount", {$subtract:[0.08, 0.01]}]},
      {$lte:["$l_discount", {$add:[0.08, 0.01]}]},
      {$lt:["$l_quantity", 24]}      
    ]
  }}},
  {$group:{
    _id: null,
    revenue: {$sum: {$multiply: ["$l_extendedprice", "$l_discount"]}}
  }}
], {allowDiskUse:true}));
