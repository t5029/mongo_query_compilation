conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.customer.explain("allPlansExecution").aggregate([
    {$match: {
	    c_acctbal: {$gt: 0.0}
    }},
    {$project: {
	    c_acctbal: 1,
	    cntrycode: {$substr:["$c_phone", 0, 2]}
    }},
    {$match: {
	    cntrycode: {$in: ['30', '17', '25', '10', '22', '15', '21'] }
    }},
    {$lookup: {
		from: "orders",
		localField: "_id",
		foreignField: "o_custkey",
		as: "c_orders"
	}},
    {$facet: {
        "sub_query": [
            {$group: {
                _id: null,
                c_acctbal_avg: {$avg: "$c_acctbal"}
            }}
        ],
        "outer_query": [
            {$group: {
                _id: "$cntrycode",
                numcust: {$sum: {$cond:[{$eq:["$c_orders", []]},1,0]}},
                totacctbal: {$sum: {$cond:[{$eq:["$c_orders", []]},"$c_acctbal",0]}},
                c_acctbals: {$push:{$cond:[{$eq:["$c_orders", []]},"$c_acctbal","$$REMOVE"]}}
            }},
        ]
    }},
    {$unwind: "$outer_query"},
    {$unwind: "$sub_query"},
    {$project:{
        _id: "$outer_query._id",
        numcust: "$outer_query.numcust",
        totacctbal: "$outer_query.totacctbal",
        c_acctbals: {
            $filter:{
                input:"$outer_query.c_acctbals",
                as:"c_acctbal",
                cond:{$lte:["$$c_acctbal", "$sub_query.c_acctbal_avg"]}
            }
        }
    }},
    {$project:{
        _id: 1,
        numcust: {$subtract:["$numcust", {$size:"$c_acctbals"}]},
        totacctbal: {
            $subtract:[
                "$totacctbal", 
                {$reduce: {
                    input: "$c_acctbals",
                    initialValue: 0,
                    in: {$add:["$$value", "$$this"]}
                }}
            ]
        },
    }},
    {$sort: {"_id": 1}}
], {allowDiskUse:true}));
