# Generate dataset

```
docker-compose exec mongo mongoimport --db tpch_1 --collection scale1 --file /tpch/1G/col.json --numInsertionWorkers 8
```

# Run experiments

```
python3 buildscripts/resmoke.py run --benchmarkRepetitions 5 --suites=benchmarks build/opt/mongo/db/catalog/compiled_query_1_tpch_bm --perfReportFile perf_report.json --reportFile benchmark_report.json 2>&1 | tee benchmark_result.log
```