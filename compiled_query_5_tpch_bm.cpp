// https://www.youtube.com/watch?v=nXaxk27zwlk
#include <benchmark/benchmark.h>
#include <memory>
#include <unordered_map>
#include <vector>

#include "mongo/db/catalog/query_compilation_benchmarks/benchmark_utils.hpp"

namespace mongo {
namespace tpch {
namespace {
void BM_Q5_S2_TPCH(benchmark::State& state) {
    SETUP();
    struct Customer {
        int c_nationkey;
    };
    struct Orders {
        int o_orderkey;
    };
    struct Lineitem {
        int l_suppkey;
        double price;
    };
    struct Supplier {
        int n_nationkey;
        std::string n_name;
    };
    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "customer"));
    Collection* orders =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "orders"));
    Collection* lineitem =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "lineitem"));
    Collection* supplier =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "supplier"));

    for (auto _ : state) {
        auto customerCursor = customer->getCursor(opCtx.get());
        auto ordersCursor = orders->getCursor(opCtx.get());
        auto lineitemCursor = lineitem->getCursor(opCtx.get());
        auto supplierCursor = supplier->getCursor(opCtx.get());

        std::unordered_map<int, Customer> customers;
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            auto cid = bson["_id"].Int();
            auto c_nationkey = bson["c_nationkey"].Int();
            customers.insert(std::make_pair(cid, Customer{c_nationkey}));
        }
        std::unordered_multimap<int, Lineitem> lineitems;
        while (auto record = lineitemCursor->next()) {
            auto bson = record.get().data.toBson();
            auto lid = bson["_id"].embeddedObject();
            auto oid = lid["l_orderkey"].Int();
            auto sid = bson["l_suppkey"].Int();
            auto l_discount = bson["l_discount"].Number();
            auto l_extendedprice = bson["l_extendedprice"].Number();
            auto price = l_extendedprice * (1 - l_discount);
            lineitems.insert(std::make_pair(oid, Lineitem{sid, price}));
        }
        std::unordered_map<int, Supplier> suppliers;
        std::string region_param = "MIDDLE EAST";
        while (auto record = supplierCursor->next()) {
            auto bson = record.get().data.toBson();
            auto s_nation = bson["s_nation"].embeddedObject();
            auto n_name = s_nation["n_name"].valueStringData().toString();
            auto n_region = s_nation["n_region"].embeddedObject();
            auto r_name = n_region["r_name"].valueStringData();
            if (r_name.toString() == region_param) {
                auto sid = bson["_id"].Int();
                auto n_nationkey = s_nation["n_nationkey"].Int();
                suppliers.insert(std::make_pair(sid, Supplier{n_nationkey, n_name}));
            }
        }
        std::unordered_map<std::string, double> groups;
        auto param1 = dateFromISOString(StringData("1992-01-01T00:00:00Z")).getValue();
        auto param2 = dateFromISOString(StringData("1993-01-01T00:00:00Z")).getValue();
        while (auto record = ordersCursor->next()) {
            auto bson = record.get().data.toBson();
            auto o_orderdate = bson["o_orderdate"].Date();
            if (o_orderdate >= param1 && o_orderdate < param2) {
                auto cid = bson["o_custkey"].Int();
                if (auto found = customers.find(cid); found != customers.end()) {
                    auto matchedCustomer = found->second;
                    auto oid = bson["_id"].Int();
                    if (auto found = lineitems.find(oid); found != lineitems.end()) {
                        auto matchedLineitems = lineitems.equal_range(oid);
                        for (auto i = matchedLineitems.first; i != matchedLineitems.second; ++i) {
                            auto matchedLineitem = i->second;
                            if (auto found = suppliers.find(matchedLineitem.l_suppkey);
                                found != suppliers.end()) {
                                auto matchedSupplier = found->second;
                                if (matchedSupplier.n_nationkey == matchedCustomer.c_nationkey) {
                                    groups[matchedSupplier.n_name] += matchedLineitem.price;
                                }
                            }
                        }
                    }
                }
            }
        }
        benchmark::DoNotOptimize(groups);
        std::vector<std::string> sorted_groups(groups.size(), "");
        std::transform(groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });
        std::sort(sorted_groups.begin(),
                  sorted_groups.end(),
                  [&groups](std::string a, std::string b) { return groups[a] > groups[b]; });
        benchmark::DoNotOptimize(sorted_groups);

        customerCursor.reset();
        ordersCursor.reset();
        lineitemCursor.reset();
        supplierCursor.reset();
    }
    CLEANUP();
}

void BM_Q5_S1_TPCH(benchmark::State& state) {
    SETUP();
    struct Supplier {
        int n_nationkey;
        std::string n_name;
    };
    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "scale1"));
    Collection* supplier =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "supplier"));

    for (auto _ : state) {
        auto customerCursor = customer->getCursor(opCtx.get());
        auto supplierCursor = supplier->getCursor(opCtx.get());

        std::unordered_map<int, Supplier> suppliers;
        std::unordered_map<int, bool> nations;
        std::string region_param = "MIDDLE EAST";
        while (auto record = supplierCursor->next()) {
            auto bson = record.get().data.toBson();
            auto s_nation = bson["s_nation"].embeddedObject();
            auto n_nationkey = s_nation["n_nationkey"].Int();
            auto n_region = s_nation["n_region"].embeddedObject();
            auto r_name = n_region["r_name"].valueStringData();
            if (r_name.toString() == region_param) {
                auto sid = bson["_id"].Int();
                auto n_name = s_nation["n_name"].valueStringData().toString();
                suppliers.insert(std::make_pair(sid, Supplier{n_nationkey, n_name}));
                nations[n_nationkey] = true;
            } else {
                benchmark::DoNotOptimize(!nations[n_nationkey]);
            }
        }
        auto param1 = dateFromISOString(StringData("1992-01-01T00:00:00Z")).getValue();
        auto param2 = dateFromISOString(StringData("1993-01-01T00:00:00Z")).getValue();
        std::unordered_map<std::string, double> groups;
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            auto c_nationkey = bson["c_nationkey"].Int();
            if (nations[c_nationkey]) {
                auto orders = bson["c_orders"].Array();
                for (const auto& order : orders) {
                    auto o_orderdate = order["o_orderdate"].Date();
                    if (o_orderdate >= param1 && o_orderdate < param2) {
                        auto lineitems = order["o_lineitems"].Array();
                        for (const auto& lineitem : lineitems) {
                            auto l_suppkey = lineitem["l_suppkey"].Int();
                            if (auto found = suppliers.find(l_suppkey); found != suppliers.end()) {
                                auto l_extendedprice = lineitem["l_extendedprice"].Number();
                                auto l_discount = lineitem["l_discount"].Number();
                                auto matchedSupplier = found->second;
                                if (matchedSupplier.n_nationkey == c_nationkey) {
                                    groups[matchedSupplier.n_name] +=
                                        l_extendedprice * (1 - l_discount);
                                }
                            }
                        }
                    }
                }
            }
        }
        benchmark::DoNotOptimize(groups);
        std::vector<std::string> sorted_groups(groups.size(), "");
        std::transform(groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });
        std::sort(sorted_groups.begin(),
                  sorted_groups.end(),
                  [&groups](std::string a, std::string b) { return groups[a] > groups[b]; });
        benchmark::DoNotOptimize(sorted_groups);
        customerCursor.reset();
        supplierCursor.reset();
    }
    CLEANUP();
}

BENCHMARK(BM_Q5_S2_TPCH)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_Q5_S1_TPCH)->Unit(benchmark::kMillisecond);

}  // namespace
}  // namespace tpch
}  // namespace mongo
