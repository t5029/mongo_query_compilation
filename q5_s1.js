conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.scale1.explain("allPlansExecution").aggregate([
  {$project:{
    c_nationkey:1,
    c_orders: {$filter: {
      input: "$c_orders",
      as: "o",
      cond: {$and: [
	{$gte:["$$o.o_orderdate", ISODate("1992-01-01T00:00:00Z")]},
	{$lt:["$$o.o_orderdate", ISODate("1993-01-01T00:00:00Z")]}
      ]}
    }}
  }},
  {$unwind: "$c_orders"},
  {$project:{
    c_nationkey:1,
    o_lineitems:"$c_orders.o_lineitems",
  }},
  {$unwind: "$o_lineitems"},
  {$lookup: {
    from: "supplier",
    localField: "o_lineitems.l_suppkey",
    foreignField: "_id",
    as: "suppliers"
  }},
  {$unwind: "$suppliers"},
  {$match: {"suppliers.s_nation.n_region.r_name": "MIDDLE EAST"}},
  {$match: {$expr:{$eq:["$suppliers.s_nation.n_nationkey", "$c_nationkey"]}}},
  {$group: {
    _id: "$suppliers.s_nation.n_name",
    revenue: {
      $sum: {
	$multiply:[
	  "$o_lineitems.l_extendedprice", 
	  {$subtract:[1, "$o_lineitems.l_discount"]}]}}
  }},
  { $sort: {"revenue": -1} }

], {allowDiskUse:true}));

