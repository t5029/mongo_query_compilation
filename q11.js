conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.supplier.explain("allPlansExecution").aggregate([
  {$match: {"s_nation.n_name": "ARGENTINA"}},
  {$lookup: {
    from: "part",
    localField: "s_parts",
    foreignField: "_id",
    as: "part"
  }},
  {$unwind: "$part"},
  {$project: {
    ps_partkey: "$part._id",
    p_suppliers:{
      $filter: {
	input: "$part.p_suppliers",
	as: "ps",
	cond: {$eq:["$_id", "$$ps.ps_suppkey"]}
    }}
  }},
  {$unwind: "$p_suppliers"},
  {$project:{
    ps_partkey: 1,
    ps_supplycost: "$p_suppliers.ps_supplycost",
    ps_availqty: "$p_suppliers.ps_availqty"
  }},
  {$facet: {
    "sub_cost": [
      {$group: {
	_id: null,
	value: {$sum:{$multiply:["$ps_supplycost", "$ps_availqty"]}}
      }},
      {$project:{
	_id:0,
	value: {$multiply:["$value", 0.0001000000]}
      }}
    ],
    "cost": [
      {$group: {
	_id: "$ps_partkey",
	value: {$sum:{$multiply:["$ps_supplycost", "$ps_availqty"]}}
      }}
    ]
  }},
  {$unwind:"$sub_cost"},
  {$unwind:"$cost"},
  {$match: {$expr:{$gt:["$cost.value", "$sub_cost.value"]}}},
  {$project: {
    ps_partkey: "$cost._id",
    value: "$cost.value"
  }},
  { $sort: {"value": -1} }
], {allowDiskUse:true}));
