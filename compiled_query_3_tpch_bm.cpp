// https://www.youtube.com/watch?v=nXaxk27zwlk
#include <benchmark/benchmark.h>
#include <chrono>
#include <memory>
#include <unordered_map>
#include <vector>

#include "mongo/db/catalog/query_compilation_benchmarks/benchmark_utils.hpp"

namespace mongo {
namespace tpch {
namespace {
void BM_Q3_S2_TIMING_HASHTABLES_TPCH(benchmark::State& state) {
    using group_key = BSONObj;
    SETUP();

    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "customer"));
    Collection* orders =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "orders"));
    Collection* lineitem =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "lineitem"));

    // time to create Lineitem hash table
    int lineitemHT = 0;
    // time to create Customer hash table
    int customerHT = 0;
    // time to scan Orders, probe Customer and Lineitem hash tables
    // and compute aggregates
    int ordersProbe = 0;
    // time to sort groups after aggregations
    int sorting = 0;

    for (auto _ : state) {
        using Customer = void*;
        struct Orders {
            int o_orderkey;
        };
        struct Lineitem {
            double price;
        };
        struct Aggs {
            double revenue;
        };

        auto customerCursor = customer->getCursor(opCtx.get());
        auto ordersCursor = orders->getCursor(opCtx.get());
        auto lineitemCursor = lineitem->getCursor(opCtx.get());

        std::string param2 = "AUTOMOBILE";
        auto customers = std::unordered_map<int, Customer>();
        auto start = std::chrono::high_resolution_clock::now();
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            auto c_mktsegment = bson["c_mktsegment"].String();
            if (c_mktsegment.compare(param2) == 0) {
                auto cid = bson["_id"].Int();
                customers[cid] = nullptr;
            }
        }
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::high_resolution_clock::now() - start);
        customerHT += duration.count();

        auto param1 = dateFromISOString(StringData("1992-01-02T00:00:00Z")).getValue();
        auto lineitems = std::unordered_multimap<int, Lineitem>();
        start = std::chrono::high_resolution_clock::now();
        while (auto record = lineitemCursor->next()) {
            auto bson = record.get().data.toBson();
            auto l_shipdate = bson["l_shipdate"].Date();
            if (l_shipdate > param1) {
                auto lid = bson["_id"].Obj();
                auto l_discount = bson["l_discount"].Number();
                auto l_extendedprice = bson["l_extendedprice"].Number();
                auto price = l_extendedprice * (1 - l_discount);

                auto l_orderkey = lid["l_orderkey"].Int();
                lineitems.insert(std::make_pair(l_orderkey, Lineitem{price}));
            }
        }
        duration = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::high_resolution_clock::now() - start);
        lineitemHT += duration.count();

        start = std::chrono::high_resolution_clock::now();
        auto groups = SimpleBSONObjComparator::kInstance.makeBSONObjIndexedUnorderedMap<Aggs>();
        while (auto record = ordersCursor->next()) {
            auto bson = record.get().data.toBson();
            auto o_orderdate = bson["o_orderdate"];
            if (o_orderdate.Date() < param1) {
                auto cid = bson["o_custkey"].Int();
                if (auto found = customers.find(cid); found != customers.end()) {
                    auto oid = bson["_id"].Int();
                    if (auto found = lineitems.find(oid); found != lineitems.end()) {
                        auto matchedLineitems = lineitems.equal_range(oid);
                        for (auto i = matchedLineitems.first; i != matchedLineitems.second; ++i) {
                            auto l_orderkey = i->first;
                            auto o_shippriority = bson["o_shippriority"];
                            BSONObjBuilder b;
                            b.appendIntOrLL("l_orderkey", l_orderkey);
                            b.append(o_orderdate);
                            b.append(o_shippriority);
                            group_key group = b.obj();

                            auto matchedLineitem = i->second;
                            Aggs& aggs = groups[group];
                            aggs.revenue += matchedLineitem.price;
                        }
                    }
                }
            }
        }
        benchmark::DoNotOptimize(groups);
        duration = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::high_resolution_clock::now() - start);
        ordersProbe += duration.count();

        start = std::chrono::high_resolution_clock::now();
        std::vector<BSONObj> sorted_groups(groups.size());
        std::transform(groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });
        auto limit = 10;
        std::partial_sort(sorted_groups.begin(),
                          sorted_groups.begin() + limit,
                          sorted_groups.end(),
                          [&groups](const BSONObj& a, const BSONObj& b) {
                              auto o_orderdate_a = a["o_orderdate"].Date();
                              auto o_orderdate_b = b["o_orderdate"].Date();
                              auto revenue_a = groups[a].revenue;
                              auto revenue_b = groups[b].revenue;
                              return revenue_a > revenue_b ||
                                  (revenue_a == revenue_b && o_orderdate_a < o_orderdate_b);
                          });
        benchmark::DoNotOptimize(sorted_groups);
        duration = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::high_resolution_clock::now() - start);
        sorting += duration.count();

        // for (auto i = sorted_groups.begin(); i < sorted_groups.end(); ++i) {
        //     if (limit < 1) {
        //         break;
        //     }
        //     printf("\nl_orderkey: %d, o_orderdate: %s, o_shippriority: %d,  revenue: %f",
        //            (*i)["l_orderkey"].Int(),
        //            (*i)["o_orderdate"].Date().toString().c_str(),
        //            (*i)["o_shippriority"].Int(),
        //            groups[*i].revenue);
        //     limit--;
        // }
        customerCursor.reset();
        ordersCursor.reset();
        lineitemCursor.reset();
    }
    state.counters["customerHT"] = benchmark::Counter(customerHT, benchmark::Counter::kAvgThreads);
    state.counters["lineitemHT"] = benchmark::Counter(lineitemHT, benchmark::Counter::kAvgThreads);
    state.counters["ordersProbe"] =
        benchmark::Counter(ordersProbe, benchmark::Counter::kAvgThreads);
    state.counters["sorting"] = benchmark::Counter(sorting, benchmark::Counter::kAvgThreads);
    CLEANUP();
}

void BM_Q3_S2_TPCH(benchmark::State& state) {
    using group_key = BSONObj;
    SETUP();

    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "customer"));
    Collection* orders =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "orders"));
    Collection* lineitem =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "lineitem"));

    for (auto _ : state) {
        using Customer = void*;
        struct Orders {
            int o_orderkey;
        };
        struct Lineitem {
            double price;
        };
        struct Aggs {
            double revenue;
        };

        auto customerCursor = customer->getCursor(opCtx.get());
        auto ordersCursor = orders->getCursor(opCtx.get());
        auto lineitemCursor = lineitem->getCursor(opCtx.get());

        std::string param2 = "AUTOMOBILE";
        auto customers = std::unordered_map<int, Customer>();
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            auto c_mktsegment = bson["c_mktsegment"].String();
            if (c_mktsegment.compare(param2) == 0) {
                auto cid = bson["_id"].Int();
                customers[cid] = nullptr;
            }
        }
        auto param1 = dateFromISOString(StringData("1992-01-02T00:00:00Z")).getValue();
        auto lineitems = std::unordered_multimap<int, Lineitem>();
        while (auto record = lineitemCursor->next()) {
            auto bson = record.get().data.toBson();
            auto l_shipdate = bson["l_shipdate"].Date();
            if (l_shipdate > param1) {
                auto lid = bson["_id"].Obj();
                auto l_discount = bson["l_discount"].Number();
                auto l_extendedprice = bson["l_extendedprice"].Number();
                auto price = l_extendedprice * (1 - l_discount);

                auto l_orderkey = lid["l_orderkey"].Int();
                lineitems.insert(std::make_pair(l_orderkey, Lineitem{price}));
            }
        }

        auto groups = SimpleBSONObjComparator::kInstance.makeBSONObjIndexedUnorderedMap<Aggs>();
        while (auto record = ordersCursor->next()) {
            auto bson = record.get().data.toBson();
            auto o_orderdate = bson["o_orderdate"];
            if (o_orderdate.Date() < param1) {
                auto cid = bson["o_custkey"].Int();
                if (auto found = customers.find(cid); found != customers.end()) {
                    auto oid = bson["_id"].Int();
                    if (auto found = lineitems.find(oid); found != lineitems.end()) {
                        auto matchedLineitems = lineitems.equal_range(oid);
                        for (auto i = matchedLineitems.first; i != matchedLineitems.second; ++i) {
                            auto l_orderkey = i->first;
                            auto o_shippriority = bson["o_shippriority"];
                            BSONObjBuilder b;
                            b.appendIntOrLL("l_orderkey", l_orderkey);
                            b.append(o_orderdate);
                            b.append(o_shippriority);
                            group_key group = b.obj();

                            auto matchedLineitem = i->second;
                            Aggs& aggs = groups[group];
                            aggs.revenue += matchedLineitem.price;
                        }
                    }
                }
            }
        }
        benchmark::DoNotOptimize(groups);
        std::vector<BSONObj> sorted_groups(groups.size());
        std::transform(groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });
        auto limit = 10;
        std::partial_sort(sorted_groups.begin(),
                          sorted_groups.begin() + limit,
                          sorted_groups.end(),
                          [&groups](const BSONObj& a, const BSONObj& b) {
                              auto o_orderdate_a = a["o_orderdate"].Date();
                              auto o_orderdate_b = b["o_orderdate"].Date();
                              auto revenue_a = groups[a].revenue;
                              auto revenue_b = groups[b].revenue;
                              return revenue_a > revenue_b ||
                                  (revenue_a == revenue_b && o_orderdate_a < o_orderdate_b);
                          });
        benchmark::DoNotOptimize(sorted_groups);

        // for (auto i = sorted_groups.begin(); i < sorted_groups.end(); ++i) {
        //     if (limit < 1) {
        //         break;
        //     }
        //     printf("\nl_orderkey: %d, o_orderdate: %s, o_shippriority: %d,  revenue: %f",
        //            (*i)["l_orderkey"].Int(),
        //            (*i)["o_orderdate"].Date().toString().c_str(),
        //            (*i)["o_shippriority"].Int(),
        //            groups[*i].revenue);
        //     limit--;
        // }
        customerCursor.reset();
        ordersCursor.reset();
        lineitemCursor.reset();
    }
    CLEANUP();
}

void BM_Q3_S1_TPCH(benchmark::State& state) {
    using group_key = BSONObj;
    SETUP();

    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "scale1"));

    for (auto _ : state) {
        struct Aggs {
            double revenue;
        };
        auto customerCursor = customer->getCursor(opCtx.get());

        auto param1 = dateFromISOString(StringData("1992-01-02T00:00:00Z")).getValue();
        std::string param2 = "AUTOMOBILE";

        auto groups = SimpleBSONObjComparator::kInstance.makeBSONObjIndexedUnorderedMap<Aggs>();
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            auto c_mktsegment = bson["c_mktsegment"].String();
            if (c_mktsegment.compare(param2) != 0 || !bson.hasField("c_orders")) {
                continue;
            }
            StringData l_orderkey_field("l_orderkey");
            auto orders = bson["c_orders"].Array();
            for (const auto& order : orders) {
                auto o_orderdate = order["o_orderdate"];
                if (o_orderdate.Date() < param1) {
                    // l_orderkey doesn't exist in S1 because
                    // lineitems are already embedded in orders,
                    // so we must use o_orderkey instead
                    auto o_orderkey = order["o_orderkey"];
                    auto o_shippriority = order["o_shippriority"];
                    auto lineitems = order["o_lineitems"].Array();
                    for (const auto& lineitem : lineitems) {
                        auto l_shipdate = lineitem["l_shipdate"].Date();
                        if (l_shipdate > param1) {
                            BSONObjBuilder b;
                            b.appendAs(o_orderkey, l_orderkey_field);
                            b.append(o_orderdate);
                            b.append(o_shippriority);
                            group_key group = b.obj();

                            Aggs& aggs = groups[group];
                            auto l_discount = lineitem["l_discount"].Number();
                            auto l_extendedprice = lineitem["l_extendedprice"].Number();
                            aggs.revenue += l_extendedprice * (1 - l_discount);
                        }
                    }
                }
            }
        }
        benchmark::DoNotOptimize(groups);
        std::vector<BSONObj> sorted_groups(groups.size());
        std::transform(groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });
        auto limit = 10;
        std::partial_sort(sorted_groups.begin(),
                          sorted_groups.begin() + limit,
                          sorted_groups.end(),
                          [&groups](BSONObj a, BSONObj b) {
                              auto o_orderdate_a = a["o_orderdate"].Date();
                              auto o_orderdate_b = b["o_orderdate"].Date();
                              auto revenue_a = groups[a].revenue;
                              auto revenue_b = groups[b].revenue;
                              return revenue_a > revenue_b ||
                                  (revenue_a == revenue_b && o_orderdate_a < o_orderdate_b);
                          });
        benchmark::DoNotOptimize(sorted_groups);

        // for (auto i = sorted_groups.begin(); i < sorted_groups.end(); ++i) {
        //     if (limit < 1) {
        //         break;
        //     }
        //     printf("\nl_orderkey: %d, o_orderdate: %s, o_shippriority: %d,  revenue: %f",
        //            (*i)["l_orderkey"].Int(),
        //            (*i)["o_orderdate"].Date().toString().c_str(),
        //            (*i)["o_shippriority"].Int(),
        //            groups[*i].revenue);
        //     --limit;
        // }
        customerCursor.reset();
    }
    CLEANUP();
}

BENCHMARK(BM_Q3_S2_TIMING_HASHTABLES_TPCH)->Unit(benchmark::kMicrosecond);
BENCHMARK(BM_Q3_S2_TPCH)->Unit(benchmark::kMicrosecond);
BENCHMARK(BM_Q3_S1_TPCH)->Unit(benchmark::kMillisecond);

}  // namespace
}  // namespace tpch
}  // namespace mongo
