#include <benchmark/benchmark.h>
#include <memory>
#include <unordered_map>
#include <vector>

#include "mongo/db/catalog/query_compilation_benchmarks/benchmark_utils.hpp"

namespace mongo {
namespace tpch {
namespace {
void BM_Q12_S2_TPCH(benchmark::State& state) {
    using group_key = std::string;
    SETUP();

    Collection* orders =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "orders"));
    Collection* lineitem =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "lineitem"));

    for (auto _ : state) {
        struct Lineitem {
            std::string l_shipmode;
        };
        struct Aggs {
            int high_line_count;
            int low_line_count;
        };

        auto ordersCursor = orders->getCursor(opCtx.get());
        auto lineitemCursor = lineitem->getCursor(opCtx.get());

        const std::string param1[2] = {"RAIL", "REG AIR"};
        const Date_t param2 = dateFromISOString(StringData("1992-01-01T00:00:00Z")).getValue();
        const Date_t param3 = dateFromISOString(StringData("1993-01-01T00:00:00Z")).getValue();

        auto lineitems = std::unordered_multimap<int, Lineitem>();
        while (auto record = lineitemCursor->next()) {
            auto bson = record.get().data.toBson();
            auto l_receiptdate = bson["l_receiptdate"].Date();
            auto l_commitdate = bson["l_commitdate"].Date();
            auto l_shipdate = bson["l_shipdate"].Date();
            auto l_shipmode = bson["l_shipmode"].String();
            auto found = std::find(std::begin(param1), std::end(param1), l_shipmode);
            if (found != std::end(param1) && l_commitdate < l_receiptdate &&
                l_shipdate < l_commitdate && l_receiptdate >= param2 && l_receiptdate < param3) {
                auto lid = bson["_id"].Obj();
                auto l_orderkey = lid["l_orderkey"].Int();
                lineitems.insert(std::make_pair(l_orderkey, Lineitem{l_shipmode}));
            }
        }

        auto groups = std::map<group_key, Aggs>();
        while (auto record = ordersCursor->next()) {
            auto bson = record.get().data.toBson();
            auto o_orderkey = bson["_id"].Int();
            if (auto found = lineitems.find(o_orderkey); found != lineitems.end()) {
                auto matchedLineitems = lineitems.equal_range(o_orderkey);
                for (auto i = matchedLineitems.first; i != matchedLineitems.second; ++i) {
                    auto o_orderpriority = bson["o_orderpriority"].String();
                    auto matchedLineitem = i->second;
                    group_key group = matchedLineitem.l_shipmode;

                    Aggs& aggs = groups[group];
                    if (o_orderpriority == "1-URGENT" || o_orderpriority == "2-HIGH") {
                        aggs.high_line_count++;
                    }
                    if (o_orderpriority != "1-URGENT" && o_orderpriority != "2-HIGH") {
                        aggs.low_line_count++;
                    }
                }
            }
        }
        benchmark::DoNotOptimize(groups);

        // for (const auto& i : groups) {
        //     printf("\nl_shipmode: %s, high_line_count: %d, low_line_count: %d",
        //            i.first.c_str(),
        //            i.second.high_line_count,
        //            i.second.low_line_count);
        // }

        ordersCursor.reset();
        lineitemCursor.reset();
    }
    CLEANUP();
}

void BM_Q12_S1_TPCH(benchmark::State& state) {
    using group_key = std::string;
    SETUP();
    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "scale1"));
    for (auto _ : state) {
        struct Aggs {
            int high_line_count;
            int low_line_count;
        };

        auto customerCursor = customer->getCursor(opCtx.get());

        const std::string param1[2] = {"RAIL", "REG AIR"};
        const Date_t param2 = dateFromISOString(StringData("1992-01-01T00:00:00Z")).getValue();
        const Date_t param3 = dateFromISOString(StringData("1993-01-01T00:00:00Z")).getValue();

        auto groups = std::map<group_key, Aggs>();
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            if (!bson.hasField("c_orders")) {
                continue;
            }
            auto orders = bson["c_orders"].Array();
            for (const auto& order : orders) {
                auto o_orderpriority = order["o_orderpriority"].String();
                auto lineitems = order["o_lineitems"].Array();
                for (const auto& lineitem : lineitems) {
                    auto l_receiptdate = lineitem["l_receiptdate"].Date();
                    auto l_commitdate = lineitem["l_commitdate"].Date();
                    auto l_shipdate = lineitem["l_shipdate"].Date();
                    auto l_shipmode = lineitem["l_shipmode"].String();
                    auto found = std::find(std::begin(param1), std::end(param1), l_shipmode);
                    if (found != std::end(param1) && l_commitdate < l_receiptdate &&
                        l_shipdate < l_commitdate && l_receiptdate >= param2 &&
                        l_receiptdate < param3) {

                        Aggs& aggs = groups[l_shipmode];
                        if (o_orderpriority == "1-URGENT" || o_orderpriority == "2-HIGH") {
                            aggs.high_line_count++;
                        }
                        if (o_orderpriority != "1-URGENT" && o_orderpriority != "2-HIGH") {
                            aggs.low_line_count++;
                        }
                    }
                }
            }
        }
        benchmark::DoNotOptimize(groups);

        // for (const auto& i : groups) {
        //     printf("\nl_shipmode: %s, high_line_count: %d, low_line_count: %d",
        //            i.first.c_str(),
        //            i.second.high_line_count,
        //            i.second.low_line_count);
        // }

        customerCursor.reset();
    }
    CLEANUP();
}

BENCHMARK(BM_Q12_S2_TPCH)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_Q12_S1_TPCH)->Unit(benchmark::kMillisecond);

}  // namespace
}  // namespace tpch
}  // namespace mongo
