// Step 3: reordering joins. Note that the join between customer and supplier was nor reordered
// due to the advantage of using an index for the $lookup of lineitem and supplier
// Time (measured using time command): 2m0.614s
conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.orders.explain("allPlansExecution").aggregate([
  {$match:{$expr:{
    $and:[
      {$gte:["$o_orderdate", ISODate("1992-01-01T00:00:00Z")]},
      {$lt:["$o_orderdate", ISODate("1993-01-01T00:00:00Z")]}
    ]
  }}},
  {$project:{_id:1, o_custkey:1}},
  {$lookup: {
    from: "customer",
    localField: "o_custkey",
    foreignField: "_id",
    as: "customers"
  }},
  //Only one customer
  {$unwind: "$customers"},
  {$project: {
    _id:1,
    "customers.c_nationkey":1
  }},
  {$lookup: {
    from: "lineitem",
    localField: "_id",
    foreignField: "_id.l_orderkey",
    as: "lineitems"
  }},
  {$unwind: "$lineitems"},
  {$project:{
    _id:0,
    "customers.c_nationkey":1,
    "lineitems.l_suppkey":1, 
    "lineitems.l_extendedprice":1,
    "lineitems.l_discount":1
  }},
  {$lookup: {
    from: "supplier",
    localField: "lineitems.l_suppkey",
    foreignField: "_id",
    as: "suppliers"
  }},
  {$unwind: "$suppliers"},
  {$match: {"suppliers.s_nation.n_region.r_name": "MIDDLE EAST"}},
  {$match: {$expr:{$eq:["$suppliers.s_nation.n_nationkey", "$customers.c_nationkey"]}}},
  {$project:{
    "suppliers.s_nation.n_region.r_name": 1,
    "suppliers.s_nation.n_name":1,
    l_extendedprice: "$lineitems.l_extendedprice",
    l_discount: "$lineitems.l_discount"
  }},
  {$group: {
    _id: "$suppliers.s_nation.n_name",
    revenue: {$sum: {$multiply:["$l_extendedprice", {$subtract:[1, "$l_discount"]}]}}
  }},
  { $sort: {"revenue": -1} }
], {allowDiskUse:true}));
