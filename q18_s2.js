conn = new Mongo();
db = conn.getDB("tpch_1");
printjson(db.lineitem.explain("allPlansExecution").aggregate([
  {$group: {
    _id: "$_id.l_orderkey",
    sum_qty: {$sum:"$l_quantity"}
  }},
  {$match: {
    sum_qty: {$gt: 313}
  }},
  {$lookup:{
    from: "orders",
    localField: "_id",
    foreignField: "_id",
    as: "order"  
  }},
  {$unwind: "$order"},
  {$lookup:{
    from: "customer",
    localField: "order.o_custkey",
    foreignField: "_id",
    as: "customer"
  }},
  {$unwind: "$customer"},
  {$project: {
    _id: "$order.o_custkey",
    c_name: "$customer.c_name",
    o_orderkey: "$_id",
    o_orderdate: "$order.o_orderdate",
    o_totalprice: "$order.o_totalprice",
    sum_qty: 1
  }},
  {$sort:{o_totalprice: -1, o_orderdate: 1}},
  {$limit: 100}
], {allowDiskUse:true}));
