#include <benchmark/benchmark.h>
#include <boost/functional/hash.hpp>
#include <cstring>
#include <functional>
#include <limits>
#include <memory>
#include <string.h>
#include <unordered_map>
#include <vector>

#include "mongo/db/catalog/query_compilation_benchmarks/benchmark_utils.hpp"

namespace mongo {
namespace tpch {
namespace {

void BM_Q6_S2_TPCH(benchmark::State& state) {
    using group_key = BSONObj;
    SETUP();
    Collection* lineitem =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "lineitem"));

    for (auto _ : state) {
        double revenue = 0;

        auto lineitemCursor = lineitem->getCursor(opCtx.get());
        Date_t param1 = dateFromISOString(StringData("1992-04-30T00:00:00Z")).getValue();
        Date_t param2 = dateFromISOString(StringData("1993-04-30T00:00:00Z")).getValue();

        auto range1 = std::make_pair(0.08 - 0.01, 0.08 + 0.01);

        double l_extendedprice, l_discount;
        int l_quantity;
        Date_t l_shipdate;
        while (auto record = lineitemCursor->next()) {
            auto bson = record.get().data.toBson();
            l_shipdate = bson["l_shipdate"].Date();
            l_discount = bson["l_discount"].Number();
            l_quantity = bson["l_quantity"].Int();
            if (l_shipdate >= param1 && l_shipdate < param2 && l_discount >= range1.first &&
                l_discount <= range1.second && l_quantity < 24) {
                l_extendedprice = bson["l_extendedprice"].Number();
                benchmark::DoNotOptimize(revenue += l_extendedprice * l_discount);
            }
        }
        //printf("\nrevenue: %f\n", revenue);
        lineitemCursor.reset();
    }
    CLEANUP();
}

void BM_Q6_S1_TPCH(benchmark::State& state) {
    using group_key = BSONObj;
    SETUP();
    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "scale1"));

    for (auto _ : state) {
        double revenue = 0;

        auto customerCursor = customer->getCursor(opCtx.get());
        Date_t param1 = dateFromISOString(StringData("1992-04-30T00:00:00Z")).getValue();
        Date_t param2 = dateFromISOString(StringData("1993-04-30T00:00:00Z")).getValue();

        auto range1 = std::make_pair(0.08 - 0.01, 0.08 + 0.01);

        double l_extendedprice, l_discount;
        int l_quantity;
        Date_t l_shipdate;
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            if (!bson.hasField("c_orders")) {
                continue;
            }
            auto orders = bson["c_orders"].Array();
            for (const auto& order : orders) {
                auto lineitems = order["o_lineitems"].Array();
                for (const auto& lineitem : lineitems) {
                    l_shipdate = lineitem["l_shipdate"].Date();
                    l_discount = lineitem["l_discount"].Number();
                    l_quantity = lineitem["l_quantity"].Int();
                    if (l_shipdate >= param1 && l_shipdate < param2 && l_discount >= range1.first &&
                        l_discount <= range1.second && l_quantity < 24) {
                        l_extendedprice = lineitem["l_extendedprice"].Number();
                        benchmark::DoNotOptimize(revenue += l_extendedprice * l_discount);
                    }
                }
            }
        }
        //printf("\nrevenue: %f\n", revenue);
        customerCursor.reset();
    }
    CLEANUP();
}

BENCHMARK(BM_Q6_S2_TPCH)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_Q6_S1_TPCH)->Unit(benchmark::kMillisecond);

}  // namespace
}  // namespace tpch
}  // namespace mongo
