#pragma once
#include "mongo/platform/basic.h"

#include "mongo/db/storage/kv/kv_engine_test_harness.h"

#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>

#include "mongo/base/init.h"
#include "mongo/base/string_data.h"
#include "mongo/db/catalog/catalog_control.h"
#include "mongo/db/catalog/collection.h"
#include "mongo/db/catalog/collection_catalog.h"
#include "mongo/db/catalog/collection_impl.h"
#include "mongo/db/catalog/database_holder_impl.h"
#include "mongo/db/catalog/index_catalog.h"
#include "mongo/db/catalog/index_key_validate.h"
#include "mongo/db/client.h"
#include "mongo/db/concurrency/d_concurrency.h"
#include "mongo/db/concurrency/lock_state.h"
#include "mongo/db/db_raii.h"
#include "mongo/db/index/index_access_method_factory_impl.h"
#include "mongo/db/op_observer_impl.h"
#include "mongo/db/op_observer_registry.h"
#include "mongo/db/operation_context.h"
#include "mongo/db/operation_context_noop.h"
#include "mongo/db/repl/repl_settings.h"
#include "mongo/db/repl/replication_coordinator_mock.h"
#include "mongo/db/repl/storage_interface_mock.h"
#include "mongo/db/s/collection_sharding_state.h"
#include "mongo/db/s/collection_sharding_state_factory_shard.h"
#include "mongo/db/server_options.h"
#include "mongo/db/service_context.h"
#include "mongo/db/service_context_test_fixture.h"
#include "mongo/db/service_entry_point_mongod.h"
#include "mongo/db/storage/control/storage_control.h"
#include "mongo/db/storage/storage_engine_init.h"
#include "mongo/db/storage/storage_options.h"
#include "mongo/db/storage/wiredtiger/wiredtiger_global_options.h"

#include "mongo/db/logical_clock.h"
#include "mongo/util/periodic_runner.h"
#include "mongo/util/periodic_runner_factory.h"

#define DBPATH "/var/tpch/db"
#define ENGINE_NAME "wiredTiger"

namespace mongo {
namespace tpch {
ServiceContext* setupServiceContext() {
    auto serviceContext = ServiceContext::make();
    auto serviceContextPtr = serviceContext.get();
    setGlobalServiceContext(std::move(serviceContext));
    return serviceContextPtr;
}

void setUpCatalog(ServiceContext* serviceContext) {
    DatabaseHolder::set(serviceContext, std::make_unique<DatabaseHolderImpl>());
    IndexAccessMethodFactory::set(serviceContext, std::make_unique<IndexAccessMethodFactoryImpl>());
    Collection::Factory::set(serviceContext, std::make_unique<CollectionImpl::FactoryImpl>());
}

void setUpReplication(ServiceContext* serviceContext) {
    repl::StorageInterface::set(serviceContext, std::make_unique<repl::StorageInterfaceMock>());
    auto replCoord = std::make_unique<repl::ReplicationCoordinatorMock>(serviceContext);
    repl::ReplicationCoordinator::set(serviceContext, std::move(replCoord));
    repl::setOplogCollectionName(serviceContext);
}

void setUpObservers(ServiceContext* serviceContext) {
    auto observerRegistry = std::make_unique<OpObserverRegistry>();
    serviceContext->setOpObserver(std::move(observerRegistry));
}

#define SETUP()                                                                          \
    auto serviceContext = setupServiceContext();                                         \
    serviceContext->setServiceEntryPoint(                                                \
        std::make_unique<ServiceEntryPointMongod>(serviceContext));                      \
    auto logicalClock = std::make_unique<LogicalClock>(serviceContext);                  \
    LogicalClock::set(serviceContext, std::move(logicalClock));                          \
    auto runner = makePeriodicRunner(serviceContext);                                    \
    serviceContext->setPeriodicRunner(std::move(runner));                                \
    setUpCatalog(serviceContext);                                                        \
    setUpReplication(serviceContext);                                                    \
    setUpObservers(serviceContext);                                                      \
    wiredTigerGlobalOptions.cacheSizeGB = 0.01;                                          \
    storageGlobalParams.dbpath = DBPATH;                                                 \
    storageGlobalParams.repair = false;                                                  \
    initializeStorageEngine(serviceContext, StorageEngineInitFlags::kNone);              \
    StorageControl::startStorageControls(serviceContext, true /*forTestOnly*/);          \
    serviceContext->getStorageEngine()->notifyStartupComplete();                         \
    ThreadClient threadClient(serviceContext);                                           \
    ServiceContext::UniqueOperationContext opCtx = threadClient->makeOperationContext(); \
    opCtx->swapLockState(std::make_unique<LockerImpl>(), WithLock::withoutLock());

#define CLEANUP()                                               \
    {                                                           \
        Lock::GlobalLock globalLk(opCtx.get(), MODE_X);         \
        auto databaseHolder = DatabaseHolder::get(opCtx.get()); \
        databaseHolder->closeAll(opCtx.get());                  \
    }                                                           \
    shutdownGlobalStorageEngineCleanly(serviceContext);         \
    serverGlobalParams.featureCompatibility.reset();
}  // namespace tpch
}  // namespace mongo