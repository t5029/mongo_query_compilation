#include <benchmark/benchmark.h>
#include <memory>
#include <unordered_map>
#include <vector>

#include "mongo/db/catalog/query_compilation_benchmarks/benchmark_utils.hpp"

namespace mongo {
namespace tpch {
namespace {

using results_key = std::pair<std::string, int>;

void BM_Q4_S2_TPCH(benchmark::State& state) {
    SETUP();
    Collection* orders =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "orders"));
    Collection* lineitem =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "lineitem"));

    for (auto _ : state) {
        struct Order {
            std::string orderpriority;
        };
        auto ordersCursor = orders->getCursor(opCtx.get());
        auto lineitemCursor = lineitem->getCursor(opCtx.get());
        std::unordered_map<std::string, int> groups;

        std::unordered_map<int, Order> orders_map;

        auto param1 = dateFromISOString(StringData("1992-01-01T00:00:00Z")).getValue();
        auto param2 = dateFromISOString(StringData("1992-04-01T00:00:00Z")).getValue();
        while (auto record = ordersCursor->next()) {
            auto bson = record.get().data.toBson();
            auto o_orderdate = bson["o_orderdate"].Date();
            if (o_orderdate >= param1 && o_orderdate < param2) {
                auto oid = bson["_id"].Int();
                auto orderpriority = bson["o_orderpriority"].String();
                orders_map.insert(std::make_pair(oid, Order{orderpriority}));
            }
        }

        while (auto record = lineitemCursor->next()) {
            auto bson = record.get().data.toBson();
            auto l_commitdate = bson["l_commitdate"].Date();
            auto l_receiptdate = bson["l_receiptdate"].Date();
            if (l_commitdate < l_receiptdate) {
                // prob the order table
                auto lid = bson["_id"].embeddedObject();
                auto oid = lid["l_orderkey"].Int();
                auto f = orders_map.find(oid);
                if (f != orders_map.end()) {
                    auto orderpriority = (f->second).orderpriority;
                    groups[orderpriority]++;
                    // remove order we can mark it instead
                    orders_map.erase(f);
                }
            }
        }

        std::vector<results_key> sorted_groups(groups.size());
        std::transform(
            groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) { return pair; });

        std::sort(sorted_groups.begin(), sorted_groups.end(), [](auto& left, auto& right) {
            return left.first < right.first;
        });

        benchmark::DoNotOptimize(sorted_groups);
        // for (auto i : sorted_groups) {
        //     printf("\no_orderpriority: %s, order_count: %d", i.first.c_str(), i.second);
        // }
        ordersCursor.reset();
        lineitemCursor.reset();
    }

    CLEANUP();
}

void BM_Q4_S1_TPCH(benchmark::State& state) {
    SETUP();
    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "scale1"));

    for (auto _ : state) {
        struct Supplier {
            int n_nationkey;
            std::string n_name;
        };
        auto customerCursor = customer->getCursor(opCtx.get());
        auto param1 = dateFromISOString(StringData("1992-01-01T00:00:00Z")).getValue();
        auto param2 = dateFromISOString(StringData("1992-04-01T00:00:00Z")).getValue();

        std::unordered_map<std::string, int> groups;
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            auto orders = bson["c_orders"].Array();
            for (const auto& order : orders) {
                auto o_orderdate = order["o_orderdate"].Date();
                if (o_orderdate >= param1 && o_orderdate < param2) {
                    auto lineitems = order["o_lineitems"].Array();
                    for (const auto& lineitem : lineitems) {
                        auto l_commitdate = lineitem["l_commitdate"].Date();
                        auto l_receiptdate = lineitem["l_receiptdate"].Date();
                        if (l_commitdate < l_receiptdate) {
                            // we have a match
                            auto orderpriority = order["o_orderpriority"].String();
                            groups[orderpriority]++;
                            break;
                        }
                    }
                }
            }

        }  // customers

        // paper idea:
        // how to  change data structures, moving data from one to another
        std::vector<results_key> sorted_groups(groups.size());
        std::transform(
            groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) { return pair; });

        std::sort(sorted_groups.begin(), sorted_groups.end(), [](auto& left, auto& right) {
            return left.first < right.first;
        });
        benchmark::DoNotOptimize(sorted_groups);
        // for (auto i : sorted_groups) {
        //     printf("\no_orderpriority: %s, order_count: %d", i.first.c_str(), i.second);
        // }
        customerCursor.reset();
    }

    CLEANUP();
}


BENCHMARK(BM_Q4_S2_TPCH)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_Q4_S1_TPCH)->Unit(benchmark::kMillisecond);

}  // namespace
}  // namespace tpch
}  // namespace mongo