#include <algorithm>
#include <benchmark/benchmark.h>
#include <memory>
#include <unordered_map>
#include <vector>

#include "mongo/db/catalog/query_compilation_benchmarks/benchmark_utils.hpp"

namespace mongo {
namespace tpch {
namespace {
void BM_Q18_S2_TPCH(benchmark::State& state) {
    SETUP();

    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "customer"));
    Collection* orders =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "orders"));
    Collection* lineitem =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "lineitem"));

    for (auto _ : state) {
        struct Aggs1 {
            double sum_qty;
        };
        struct Customer {
            std::string c_name;
        };
        using group2_key = BSONObj;
        struct Aggs2 {
            double sum_qty;
        };

        auto customerCursor = customer->getCursor(opCtx.get());
        auto ordersCursor = orders->getCursor(opCtx.get());
        auto lineitemCursor = lineitem->getCursor(opCtx.get());

        const int param1 = 313;

        auto groups1 = std::unordered_map<int, Aggs1>();
        while (auto record = lineitemCursor->next()) {
            auto bson = record.get().data.toBson();
            auto lid = bson["_id"].Obj();
            auto l_orderkey = lid["l_orderkey"].Int();
            auto l_quantity = bson["l_quantity"].Number();
            Aggs1& aggs = groups1[l_orderkey];
            aggs.sum_qty += l_quantity;
        }
        benchmark::DoNotOptimize(groups1);

        std::unordered_map<int, Customer> customers;
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            auto c_custkey = bson["_id"].Int();
            auto c_name = bson["c_name"].String();
            customers.insert(std::make_pair(c_custkey, Customer{c_name}));
        }

        auto groups2 = SimpleBSONObjComparator::kInstance.makeBSONObjIndexedUnorderedMap<Aggs2>();
        while (auto record = ordersCursor->next()) {
            auto bson = record.get().data.toBson();
            // stats would tell us that all orders have the field
            // o_custkey, but the system doesn't know it's a
            // foreign key. Thus, we still need to check if it's
            // on the hashtable.
            auto o_custkey = bson["o_custkey"].Int();
            auto o_orderkey = bson["_id"].Int();

            auto found1 = groups1.find(o_orderkey);
            if (found1 != groups1.end()) {
                auto sum_qty = groups1[o_orderkey].sum_qty;
                auto found2 = customers.find(o_custkey);
                if (sum_qty > param1 && found2 != customers.end()) {
                    const auto& matchedCustomer = customers[o_custkey];
                    auto c_name = matchedCustomer.c_name;
                    auto o_orderdate = bson["o_orderdate"];
                    auto o_totalprice = bson["o_totalprice"];
                    BSONObjBuilder b;
                    b.append("c_name", c_name);
                    b.append("c_custkey", o_custkey);
                    b.append("o_orderkey", o_orderkey);
                    b.append(o_orderdate);
                    b.append(o_totalprice);
                    group2_key group = b.obj();

                    Aggs2& aggs = groups2[group];
                    aggs.sum_qty = sum_qty;
                }
            }
        }
        benchmark::DoNotOptimize(groups2);

        std::vector<BSONObj> sorted_groups(groups2.size());
        std::transform(groups2.begin(), groups2.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });
        auto limit = std::min(size_t(100), groups2.size());
        std::partial_sort(sorted_groups.begin(),
                          sorted_groups.begin() + limit,
                          sorted_groups.end(),
                          [](BSONObj& a, BSONObj& b) {
                              auto o_totalprice_a = a["o_totalprice"].Number();
                              auto o_totalprice_b = b["o_totalprice"].Number();
                              auto o_orderdate_a = a["o_orderdate"].Date();
                              auto o_orderdate_b = b["o_orderdate"].Date();
                              return o_totalprice_a > o_totalprice_b ||
                                  (o_totalprice_a == o_totalprice_b &&
                                   o_orderdate_a < o_orderdate_b);
                          });
        benchmark::DoNotOptimize(sorted_groups);

        // for (auto i = sorted_groups.begin(); i < sorted_groups.end(); ++i) {
        //     if (limit < 1) {
        //         break;
        //     }
        //     printf("\n%s, sum_qty: %f", i->toString().c_str(), groups2[(*i)].sum_qty);
        //     --limit;
        // }

        customerCursor.reset();
        ordersCursor.reset();
        lineitemCursor.reset();
    }
    CLEANUP();
}

void BM_Q18_S1_TPCH(benchmark::State& state) {
    SETUP();

    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "scale1"));

    for (auto _ : state) {
        using group_key = BSONObj;
        struct Aggs {
            double sum_qty;
        };
        auto customerCursor = customer->getCursor(opCtx.get());

        const double param1 = 313;

        auto groups = SimpleBSONObjComparator::kInstance.makeBSONObjIndexedUnorderedMap<Aggs>();
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            auto c_custkey = bson["_id"];
            if (bson.hasField("c_orders")) {
                auto c_name = bson["c_name"];
                auto orders = bson["c_orders"].Array();
                for (const auto& order : orders) {
                    auto lineitems = order["o_lineitems"].Array();
                    double sum_qty = 0;
                    for (const auto& lineitem : lineitems) {
                        sum_qty += lineitem["l_quantity"].Number();
                    }
                    if (sum_qty > param1) {
                        auto o_orderkey = order["o_orderkey"];
                        auto o_orderdate = order["o_orderdate"];
                        auto o_totalprice = order["o_totalprice"];
                        BSONObjBuilder b;
                        b.append(c_name);
                        b.append(c_custkey);
                        b.append(o_orderkey);
                        b.append(o_orderdate);
                        b.append(o_totalprice);
                        group_key group = b.obj();

                        Aggs& aggs = groups[group];
                        aggs.sum_qty = sum_qty;
                    }
                }
            }
        }
        benchmark::DoNotOptimize(groups);

        std::vector<BSONObj> sorted_groups(groups.size());
        std::transform(groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });
        auto limit = std::min(size_t(100), groups.size());
        std::partial_sort(sorted_groups.begin(),
                          sorted_groups.begin() + limit,
                          sorted_groups.end(),
                          [](BSONObj& a, BSONObj& b) {
                              auto o_totalprice_a = a["o_totalprice"].Number();
                              auto o_totalprice_b = b["o_totalprice"].Number();
                              auto o_orderdate_a = a["o_orderdate"].Date();
                              auto o_orderdate_b = b["o_orderdate"].Date();
                              return o_totalprice_a > o_totalprice_b ||
                                  (o_totalprice_a == o_totalprice_b &&
                                   o_orderdate_a < o_orderdate_b);
                          });
        benchmark::DoNotOptimize(sorted_groups);

        // for (auto i = sorted_groups.begin(); i < sorted_groups.end(); ++i) {
        //     if (limit < 1) {
        //         break;
        //     }
        //     printf("\n%s, sum_qty: %f", i->toString().c_str(), groups[(*i)].sum_qty);
        //     --limit;
        // }

        customerCursor.reset();
    }
    CLEANUP();
}

BENCHMARK(BM_Q18_S2_TPCH)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_Q18_S1_TPCH)->Unit(benchmark::kMillisecond);

}  // namespace
}  // namespace tpch
}  // namespace mongo
