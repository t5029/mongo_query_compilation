#include <benchmark/benchmark.h>
#include <cstring>
#include <functional>
#include <limits>
#include <memory>
#include <regex>
#include <string.h>
#include <unordered_map>
#include <vector>

#include "mongo/db/catalog/query_compilation_benchmarks/benchmark_utils.hpp"

namespace mongo {
namespace tpch {
namespace {
void BM_Q2_TPCH(benchmark::State& state) {
    SETUP();
    Collection* supplier =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "supplier"));
    Collection* part =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "part"));

    for (auto _ : state) {
        struct Supplier {
            int n_nationkey;
            std::string n_name;
            double s_acctbal;
            std::string s_address;
            std::string s_phone;
            std::string s_comment;
            std::string s_name;
        };
        struct Result {
            double s_acctbal;
            std::string s_name;
            std::string n_name;
            int p_partkey;
            std::string p_mfgr;
            std::string s_address;
            std::string s_phone;
            std::string s_comment;
            double ps_supplycost;
        };
        auto supplierCursor = supplier->getCursor(opCtx.get());
        auto partCursor = part->getCursor(opCtx.get());

        // r_name
        std::string param1 = "MIDDLE EAST";
        // p_size
        int param2 = 38;
        // p_type
        auto param3 = std::regex("(.*)(TIN)");

        std::unordered_map<int, Supplier> suppliers;
        while (auto record = supplierCursor->next()) {
            auto bson = record.get().data.toBson();
            auto s_nation = bson["s_nation"].Obj();
            auto n_region = s_nation["n_region"].Obj();
            auto r_name = n_region["r_name"].String();
            if (r_name == param1) {
                auto n_nationkey = s_nation["n_nationkey"].Int();
                auto n_name = s_nation["n_name"].String();
                auto s_suppkey = bson["_id"].Int();
                auto s_acctbal = bson["s_acctbal"].Number();
                auto s_address = bson["s_address"].String();
                auto s_phone = bson["s_phone"].String();
                auto s_comment = bson["s_comment"].String();
                auto s_name = bson["s_name"].String();

                suppliers.insert(std::make_pair(
                    s_suppkey,
                    Supplier{
                        n_nationkey, n_name, s_acctbal, s_address, s_phone, s_comment, s_name}));
            }
        }
        std::vector<Result> res;
        while (auto record = partCursor->next()) {
            auto bson = record.get().data.toBson();
            auto p_partkey = bson["_id"].Int();

            auto p_size = bson["p_size"].Int();
            auto p_type = bson["p_type"].String();
            auto p_mfgr = bson["p_mfgr"].String();
            auto p_suppliers = bson["p_suppliers"].Array();

            auto selection_holds = (p_size == param2 && std::regex_match(p_type, param3));
            if (!selection_holds) {
                continue;
            }
            auto min_supplycost = std::numeric_limits<double>::max();
            for (const auto& s : p_suppliers) {
                auto ps_suppkey = s["ps_suppkey"].Int();
                auto ps_supplycost = s["ps_supplycost"].Number();
                auto in_suppliers = (suppliers.find(ps_suppkey) != suppliers.end());

                if (in_suppliers && ps_supplycost < min_supplycost) {
                    min_supplycost = ps_supplycost;
                }
            }

            for (const auto& s : p_suppliers) {
                auto ps_supplycost = s["ps_supplycost"].Number();
                auto ps_suppkey = s["ps_suppkey"].Int();
                auto in_suppliers = (suppliers.find(ps_suppkey) != suppliers.end());
                if (in_suppliers && ps_supplycost == min_supplycost) {
                    auto supp = suppliers[ps_suppkey];
                    res.emplace_back(Result{supp.s_acctbal,
                                            supp.s_name,
                                            supp.n_name,
                                            p_partkey,
                                            p_mfgr,
                                            supp.s_address,
                                            supp.s_phone,
                                            supp.s_comment,
                                            ps_supplycost});
                }
            }
        }
        int limit = 100;
        std::partial_sort(
            res.begin(), res.begin() + limit, res.end(), [](const Result& a, const Result& b) {
                return (a.s_acctbal > b.s_acctbal) ||
                    ((a.s_acctbal == b.s_acctbal) &&
                     (std::tie(a.n_name, a.s_name, a.p_partkey) <
                      std::tie(b.n_name, b.s_name, b.p_partkey)));
            });

        // for (const auto& r : res) {
        //     printf(
        //         "s_acctbal: %f, "
        //         "s_name: %s, "
        //         "n_name: %s, "
        //         "p_partkey: %d, "
        //         "p_mfgr: %s, "
        //         "s_address: %s, "
        //         "s_phone: %s, "
        //         "s_comment: %s, "
        //         "ps_supplycost:%f \n",
        //         r.s_acctbal,
        //         r.s_name.c_str(),
        //         r.n_name.c_str(),
        //         r.p_partkey,
        //         r.p_mfgr.c_str(),
        //         r.s_address.c_str(),
        //         r.s_phone.c_str(),
        //         r.s_comment.c_str(),
        //         r.ps_supplycost);
        //     if (0 == --limit) {
        //         break;
        //     }
        // }

        supplierCursor.reset();
        partCursor.reset();
    }

    CLEANUP();
}

BENCHMARK(BM_Q2_TPCH)->Unit(benchmark::kMillisecond);

}  // namespace
}  // namespace tpch
}  // namespace mongo