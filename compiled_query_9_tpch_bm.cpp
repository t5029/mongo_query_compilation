#include <benchmark/benchmark.h>
#include <memory>
#include <regex>
#include <unordered_map>
#include <vector>

#include "mongo/db/catalog/query_compilation_benchmarks/benchmark_utils.hpp"

namespace mongo {
namespace tpch {
namespace {
void BM_Q9_S2_TPCH(benchmark::State& state) {
    SETUP();

    Collection* orders =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "orders"));
    Collection* lineitem =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "lineitem"));
    Collection* supplier =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "supplier"));
    Collection* part =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "part"));

    for (auto _ : state) {
        using group_key = BSONObj;
        struct Aggs {
            double sum_profit;
        };
        struct Order {
            int o_year;
        };
        struct Lineitem {
            int l_suppkey;
            int l_partkey;
            int l_quantity;
            int price;
        };
        // OPTIMIZATION PATTERN:
        // Convert BSON array to atomic type array before join:
        // * DESCRIPTION:
        //   * s_parts can be casted to an array of ints because the query
        //     checks "p_partkey in s_parts", so we know a comparison with
        //     non-int elements will be false.
        //   * Non-ints can be removed from s_parts when creating the hashtable,
        //     which can potentially reduce elements of an heterogeneous array.
        // * TRIGGERED BY:
        //   * projection
        //   * narrow selection
        // * MUST GATHER STATS ON:
        //   * Whether array is heterogenous or not.
        //   * Average size of "s_parts".
        //   * Standard deviation and coefficient of variation.
        // * AVOID WHEN:
        //   * Projecting big homogeneous arrays without a narrow selection
        struct Supplier {
            int n_nationkey;
            std::string n_name;
            std::vector<int> s_parts;
        };
        struct PartSupp {
            int ps_suppkey;
            double ps_supplycost;
        };
        // OPTIMIZATION FENCE:
        // We cannot assume ps_suppkey is unique within each p_suppliers array,
        // which forces us to project it as an array rather than using a hash table
        struct Part {
            std::vector<PartSupp> p_suppliers;
        };
        auto ordersCursor = orders->getCursor(opCtx.get());
        auto lineitemCursor = lineitem->getCursor(opCtx.get());
        auto supplierCursor = supplier->getCursor(opCtx.get());
        auto partCursor = part->getCursor(opCtx.get());

        auto param1 = std::regex("(.*)(yellow)(.*)");

        std::unordered_map<int, Part> parts;
        while (auto record = partCursor->next()) {
            auto bson = record.get().data.toBson();
            auto p_name = bson["p_name"].String();
            if (std::regex_match(p_name, param1)) {
                auto p_partkey = bson["_id"].Int();
                auto p_suppliers = bson["p_suppliers"].Array();
                std::vector<PartSupp> supps(p_suppliers.size());
                for (const auto& s : p_suppliers) {
                    auto ps_suppkey = s["ps_suppkey"].Int();
                    auto ps_supplycost = s["ps_supplycost"].Number();
                    supps.emplace_back(PartSupp{ps_suppkey, ps_supplycost});
                }
                parts.insert(std::make_pair(p_partkey, Part{supps}));
            }
        }

        std::unordered_map<int, Supplier> suppliers;
        while (auto record = supplierCursor->next()) {
            auto bson = record.get().data.toBson();
            auto s_suppkey = bson["_id"].Int();
            auto s_nation = bson["s_nation"].Obj();
            auto n_nationkey = s_nation["n_nationkey"].Int();
            auto n_name = s_nation["n_name"].String();
            auto s_parts = bson["s_parts"].Array();
            std::vector<int> casted_parts(s_parts.size());
            std::transform(s_parts.begin(), s_parts.end(), casted_parts.begin(), [](auto p) {
                return p.Int();
            });
            suppliers.insert(
                std::make_pair(s_suppkey, Supplier{n_nationkey, n_name, casted_parts}));
        }

        struct tm t;
        std::unordered_map<int, Order> orders;
        while (auto record = ordersCursor->next()) {
            auto bson = record.get().data.toBson();
            auto o_orderkey = bson["_id"].Int();
            auto o_orderdate = bson["o_orderdate"].Date();
            time_t_to_Struct(o_orderdate.toTimeT(), &t);
            auto o_year = t.tm_year;
            orders.insert(std::make_pair(o_orderkey, Order{o_year}));
        }

        auto groups = SimpleBSONObjComparator::kInstance.makeBSONObjIndexedUnorderedMap<Aggs>();
        while (auto record = lineitemCursor->next()) {
            auto bson = record.get().data.toBson();
            auto l_partkey = bson["l_partkey"].Int();
            if (auto found = parts.find(l_partkey); found != parts.end()) {
                auto l_suppkey = bson["l_suppkey"].Int();
                if (auto found = suppliers.find(l_suppkey); found != suppliers.end()) {
                    auto s_parts = suppliers[l_suppkey].s_parts;
                    // Wide selection
                    auto in_parts = std::find(s_parts.begin(), s_parts.end(), l_partkey);
                    auto lid = bson["_id"].Obj();
                    auto l_orderkey = lid["l_orderkey"].Int();
                    if (auto found = orders.find(l_orderkey);
                        found != orders.end() && in_parts != s_parts.end()) {
                        auto l_discount = bson["l_discount"].Number();
                        auto l_extendedprice = bson["l_extendedprice"].Number();
                        auto l_quantity = bson["l_quantity"].Int();
                        auto price = l_extendedprice * (1 - l_discount);
                        auto p_suppliers = parts[l_partkey].p_suppliers;
                        auto n_name = suppliers[l_suppkey].n_name;
                        auto o_year = orders[l_orderkey].o_year;
                        for (const auto& supp : p_suppliers) {
                            if (supp.ps_suppkey == l_suppkey) {
                                auto amount = price - supp.ps_supplycost * l_quantity;
                                BSONObjBuilder b;
                                b.append("nation", n_name);
                                b.append("o_year", o_year);
                                group_key group = b.obj();

                                Aggs& agg = groups[group];
                                agg.sum_profit += amount;
                            }
                        }
                    }
                }
            }
        }

        benchmark::DoNotOptimize(groups);
        std::vector<BSONObj> sorted_groups(groups.size());
        std::transform(groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });
        std::sort(sorted_groups.begin(), sorted_groups.end(), [](BSONObj a, BSONObj b) {
            auto nation_a = a["nation"].String();
            auto o_year_a = a["o_year"].Int();
            auto nation_b = b["nation"].String();
            auto o_year_b = b["o_year"].Int();
            return nation_a < nation_b || (nation_a == nation_b && o_year_a > o_year_b);
        });
        benchmark::DoNotOptimize(sorted_groups);

        // for (auto i = sorted_groups.begin(); i < sorted_groups.end(); ++i) {
        //     printf("\n%s, sum_profit:%f", i->toString().c_str(), groups[*i].sum_profit);
        // }

        ordersCursor.reset();
        lineitemCursor.reset();
        supplierCursor.reset();
        partCursor.reset();
    }
    CLEANUP();
}

void BM_Q9_S1_TPCH(benchmark::State& state) {
    SETUP();

    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "scale1"));
    Collection* supplier =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "supplier"));
    Collection* part =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "part"));

    for (auto _ : state) {
        using group_key = BSONObj;
        struct Aggs {
            double sum_profit;
        };
        struct Order {
            int o_year;
        };
        struct Lineitem {
            int l_suppkey;
            int l_partkey;
            int l_quantity;
            int price;
        };
        // OPTIMIZATION PATTERN:
        // Convert BSON array to atomic type array before join:
        // * DESCRIPTION:
        //   * s_parts can be casted to an array of ints because the query
        //     checks "p_partkey in s_parts", so we know a comparison with
        //     non-int elements will be false.
        //   * Non-ints can be removed from s_parts when creating the hashtable,
        //     which can potentially reduce elements of an heterogeneous array.
        // * TRIGGERED BY:
        //   * projection
        //   * narrow selection
        // * MUST GATHER STATS ON:
        //   * Whether array is heterogenous or not.
        //   * Average size of "s_parts".
        //   * Standard deviation and coefficient of variation.
        // * AVOID WHEN:
        //   * Projecting big homogeneous arrays without a narrow selection
        struct Supplier {
            int n_nationkey;
            std::string n_name;
            std::vector<int> s_parts;
        };
        struct PartSupp {
            int ps_suppkey;
            double ps_supplycost;
        };
        // OPTIMIZATION FENCE:
        // We cannot assume ps_suppkey is unique within each p_suppliers array,
        // which forces us to project it as an array rather than using a hash table
        struct Part {
            std::vector<PartSupp> p_suppliers;
        };
        auto customerCursor = customer->getCursor(opCtx.get());
        auto supplierCursor = supplier->getCursor(opCtx.get());
        auto partCursor = part->getCursor(opCtx.get());

        auto param1 = std::regex("(.*)(yellow)(.*)");

        std::unordered_map<int, Part> parts;
        while (auto record = partCursor->next()) {
            auto bson = record.get().data.toBson();
            auto p_name = bson["p_name"].String();
            if (std::regex_match(p_name, param1)) {
                auto p_partkey = bson["_id"].Int();
                auto p_suppliers = bson["p_suppliers"].Array();
                std::vector<PartSupp> supps(p_suppliers.size());
                for (const auto& s : p_suppliers) {
                    auto ps_suppkey = s["ps_suppkey"].Int();
                    auto ps_supplycost = s["ps_supplycost"].Number();
                    supps.emplace_back(PartSupp{ps_suppkey, ps_supplycost});
                }
                parts.insert(std::make_pair(p_partkey, Part{supps}));
            }
        }

        std::unordered_map<int, Supplier> suppliers;
        while (auto record = supplierCursor->next()) {
            auto bson = record.get().data.toBson();
            auto s_suppkey = bson["_id"].Int();
            auto s_nation = bson["s_nation"].Obj();
            auto n_nationkey = s_nation["n_nationkey"].Int();
            auto n_name = s_nation["n_name"].String();
            auto s_parts = bson["s_parts"].Array();
            std::vector<int> casted_parts(s_parts.size());
            std::transform(s_parts.begin(), s_parts.end(), casted_parts.begin(), [](auto p) {
                return p.Int();
            });
            suppliers.insert(
                std::make_pair(s_suppkey, Supplier{n_nationkey, n_name, casted_parts}));
        }

        struct tm t;
        auto groups = SimpleBSONObjComparator::kInstance.makeBSONObjIndexedUnorderedMap<Aggs>();
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            if (bson.hasField("c_orders")) {
                auto orders = bson["c_orders"].Array();
                for (const auto& order : orders) {
                    auto o_orderdate = order["o_orderdate"].Date();
                    time_t_to_Struct(o_orderdate.toTimeT(), &t);
                    auto o_year = t.tm_year;
                    auto lineitems = order["o_lineitems"].Array();
                    for (const auto& lineitem : lineitems) {
                        auto l_partkey = lineitem["l_partkey"].Int();
                        if (auto found = parts.find(l_partkey); found != parts.end()) {
                            auto l_suppkey = lineitem["l_suppkey"].Int();
                            if (auto found = suppliers.find(l_suppkey); found != suppliers.end()) {
                                auto s_parts = suppliers[l_suppkey].s_parts;
                                // Wide selection
                                auto in_parts =
                                    std::find(s_parts.begin(), s_parts.end(), l_partkey);
                                if (in_parts != s_parts.end()) {
                                    auto l_discount = lineitem["l_discount"].Number();
                                    auto l_extendedprice = lineitem["l_extendedprice"].Number();
                                    auto l_quantity = lineitem["l_quantity"].Int();
                                    auto price = l_extendedprice * (1 - l_discount);
                                    auto p_suppliers = parts[l_partkey].p_suppliers;
                                    auto n_name = suppliers[l_suppkey].n_name;
                                    for (const auto& supp : p_suppliers) {
                                        if (supp.ps_suppkey == l_suppkey) {
                                            auto amount = price - supp.ps_supplycost * l_quantity;
                                            BSONObjBuilder b;
                                            b.append("nation", n_name);
                                            b.append("o_year", o_year);
                                            group_key group = b.obj();

                                            Aggs& agg = groups[group];
                                            agg.sum_profit += amount;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        benchmark::DoNotOptimize(groups);
        std::vector<BSONObj> sorted_groups(groups.size());
        std::transform(groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });
        std::sort(sorted_groups.begin(), sorted_groups.end(), [](BSONObj a, BSONObj b) {
            auto nation_a = a["nation"].String();
            auto o_year_a = a["o_year"].Int();
            auto nation_b = b["nation"].String();
            auto o_year_b = b["o_year"].Int();
            return nation_a < nation_b || (nation_a == nation_b && o_year_a > o_year_b);
        });
        benchmark::DoNotOptimize(sorted_groups);

        // for (auto i = sorted_groups.begin(); i < sorted_groups.end(); ++i) {
        //     printf("\n%s, sum_profit:%f", i->toString().c_str(), groups[*i].sum_profit);
        // }

        customerCursor.reset();
        supplierCursor.reset();
        partCursor.reset();
    }
    CLEANUP();
}

BENCHMARK(BM_Q9_S2_TPCH)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_Q9_S1_TPCH)->Unit(benchmark::kMillisecond);

}  // namespace
}  // namespace tpch
}  // namespace mongo