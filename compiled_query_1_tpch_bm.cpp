// https://www.youtube.com/watch?v=nXaxk27zwlk
#include <benchmark/benchmark.h>
#include <boost/functional/hash.hpp>
#include <cstring>
#include <functional>
#include <limits>
#include <memory>
#include <string.h>
#include <unordered_map>
#include <vector>

#include "mongo/db/catalog/query_compilation_benchmarks/benchmark_utils.hpp"

namespace mongo {
namespace tpch {
namespace {

template <typename T1, typename T2>
struct AVG {
    T1 val;
    T2 count;
};

void BM_Q1_S2_TPCH(benchmark::State& state) {
    using group_key = std::pair<std::string, std::string>;
    SETUP();
    Collection* lineitem =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "lineitem"));

    for (auto _ : state) {
        struct Aggs {
            int sum_qty;
            double sum_base_price;
            double sum_disc_price;
            double sum_charge;
            AVG<int, int> avg_qty;
            AVG<double, int> avg_price;
            AVG<double, int> avg_disc;
            int count_order;
        };
        auto lineitemCursor = lineitem->getCursor(opCtx.get());

        std::unordered_map<group_key, Aggs, boost::hash<group_key>> groups;
        auto param1 = dateFromISOString(StringData("1992-01-02T00:00:00Z")).getValue();
        while (auto record = lineitemCursor->next()) {
            auto bson = record.get().data.toBson();
            auto l_shipdate = bson["l_shipdate"].Date();
            if (l_shipdate <= param1) {
                auto l_returnflag = bson["l_returnflag"].String();
                auto l_linestatus = bson["l_linestatus"].String();
                auto l_quantity = bson["l_quantity"].Int();
                auto l_extendedprice = bson["l_extendedprice"].Number();
                auto l_discount = bson["l_discount"].Number();
                auto l_tax = bson["l_tax"].Number();
                Aggs& agg = groups[group_key(l_returnflag, l_linestatus)];
                agg.sum_qty += l_quantity;
                agg.sum_base_price += l_extendedprice;
                auto disc_price = l_extendedprice * (1 - l_discount);
                agg.sum_disc_price += disc_price;
                agg.sum_charge += disc_price * (1 + l_tax);
                agg.avg_qty.val += l_quantity;
                agg.avg_qty.count++;
                agg.avg_price.val += l_extendedprice;
                agg.avg_price.count++;
                agg.avg_disc.val += l_discount;
                agg.avg_disc.count++;
                agg.count_order++;
            }
        }
        benchmark::DoNotOptimize(groups);
        std::vector<group_key> sorted_groups(groups.size());
        std::transform(groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });

        // pairs in std::pair are compared lexicographically by default,
        // by pair 1 first and pair 2 second.
        std::sort(sorted_groups.begin(), sorted_groups.end());
        benchmark::DoNotOptimize(sorted_groups);
        // for (auto i : sorted_groups) {
        //     printf("\nl_returnflag: %s, l_linestatus: %s, %d, %f, %f, %f, %d, %f, %f, %d",
        //            i.first.c_str(),
        //            i.second.c_str(),
        //            groups[i].sum_qty,
        //            groups[i].sum_base_price,
        //            groups[i].sum_disc_price,
        //            groups[i].sum_charge,
        //            groups[i].avg_qty.val / groups[i].avg_qty.count,
        //            groups[i].avg_price.val / groups[i].avg_price.count,
        //            groups[i].avg_disc.val / groups[i].avg_disc.count,
        //            groups[i].count_order);
        // }
        lineitemCursor.reset();
    }
    CLEANUP();
}

void BM_Q1_S2_BINARY_TPCH(benchmark::State& state) {
    using group_key = BSONObj;
    SETUP();
    Collection* lineitem =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "lineitem"));

    for (auto _ : state) {
        struct Aggs {
            int sum_qty;
            double sum_base_price;
            double sum_disc_price;
            double sum_charge;
            AVG<int, int> avg_qty;
            AVG<double, int> avg_price;
            AVG<double, int> avg_disc;
            int count_order;
        };
        auto lineitemCursor = lineitem->getCursor(opCtx.get());
        auto groups = SimpleBSONObjComparator::kInstance.makeBSONObjIndexedUnorderedMap<Aggs>();
        Date_t param1 = dateFromISOString(StringData("1992-01-02T00:00:00Z")).getValue();

        double l_extendedprice, l_discount, l_tax;
        int l_quantity;
        Date_t l_shipdate;
        BSONElement l_returnflag, l_linestatus;
        while (auto record = lineitemCursor->next()) {
            auto bson = record.get().data.toBson();
            try {
                l_shipdate = bson["l_shipdate"].Date();
            } catch (...) {
                continue;
            }
            if (l_shipdate <= param1) {
                try {
                    l_quantity = bson["l_quantity"].Int();
                    l_extendedprice = bson["l_extendedprice"].Number();
                    l_discount = bson["l_discount"].Number();
                    l_tax = bson["l_tax"].Number();
                } catch (...) {
                    continue;
                }

                l_returnflag = bson["l_returnflag"];
                l_linestatus = bson["l_linestatus"];
                BSONObjBuilder b(l_returnflag.size() + l_linestatus.size());
                b.append(l_returnflag);
                b.append(l_linestatus);
                group_key group = b.obj();

                Aggs& agg = groups[group];
                agg.sum_qty += l_quantity;
                agg.sum_base_price += l_extendedprice;
                double disc_price = l_extendedprice * (1 - l_discount);
                agg.sum_disc_price += disc_price;
                agg.sum_charge += disc_price * (1 + l_tax);
                agg.avg_qty.val += l_quantity;
                agg.avg_qty.count++;
                agg.avg_price.val += l_extendedprice;
                agg.avg_price.count++;
                agg.avg_disc.val += l_discount;
                agg.avg_disc.count++;
                agg.count_order++;
            }
        }
        benchmark::DoNotOptimize(groups);
        std::vector<group_key> sorted_groups(groups.size());
        std::transform(groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });

        std::sort(sorted_groups.begin(),
                  sorted_groups.end(),
                  SimpleBSONObjComparator::kInstance.makeLessThan());
        benchmark::DoNotOptimize(sorted_groups);
        // for (auto i : sorted_groups) {
        //     printf("\nl_returnflag: %s, l_linestatus: %s, %d, %f, %f, %f, %d, %f, %f, %d",
        //            i["l_returnflag"].String().c_str(),
        //            i["l_linestatus"].String().c_str(),
        //            groups[i].sum_qty,
        //            groups[i].sum_base_price,
        //            groups[i].sum_disc_price,
        //            groups[i].sum_charge,
        //            groups[i].avg_qty.val / groups[i].avg_qty.count,
        //            groups[i].avg_price.val / groups[i].avg_price.count,
        //            groups[i].avg_disc.val / groups[i].avg_disc.count,
        //            groups[i].count_order);
        // }
        lineitemCursor.reset();
    }
    CLEANUP();
}

void BM_Q1_S1_TPCH(benchmark::State& state) {
    using group_key = std::pair<std::string, std::string>;
    SETUP();
    Collection* customer =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "scale1"));

    for (auto _ : state) {
        struct Aggs {
            int sum_qty;
            double sum_base_price;
            double sum_disc_price;
            double sum_charge;
            AVG<int, int> avg_qty;
            AVG<double, int> avg_price;
            AVG<double, int> avg_disc;
            int count_order;
        };
        auto customerCursor = customer->getCursor(opCtx.get());

        std::unordered_map<group_key, Aggs, boost::hash<group_key>> groups;
        auto param1 = dateFromISOString(StringData("1992-01-02T00:00:00Z")).getValue();
        while (auto record = customerCursor->next()) {
            auto bson = record.get().data.toBson();
            if (!bson.hasField("c_orders")) {
                continue;
            }
            auto c_orders = bson["c_orders"].Array();
            if (c_orders.empty()) {
                continue;
            }
            for (const auto& order : c_orders) {
                // Statistics tell us all orders have at least 1 lineitem embedded
                auto o_lineitems = order["o_lineitems"].Array();
                for (const auto& lineitem : o_lineitems) {
                    auto l_shipdate = lineitem["l_shipdate"].Date();
                    if (l_shipdate <= param1) {
                        auto l_returnflag = lineitem["l_returnflag"].String();
                        auto l_linestatus = lineitem["l_linestatus"].String();
                        auto l_quantity = lineitem["l_quantity"].Int();
                        auto l_extendedprice = lineitem["l_extendedprice"].Number();
                        auto l_discount = lineitem["l_discount"].Number();
                        auto l_tax = lineitem["l_tax"].Number();
                        Aggs& agg = groups[group_key(l_returnflag, l_linestatus)];
                        agg.sum_qty += l_quantity;
                        agg.sum_base_price += l_extendedprice;
                        auto disc_price = l_extendedprice * (1 - l_discount);
                        agg.sum_disc_price += disc_price;
                        agg.sum_charge += disc_price * (1 + l_tax);
                        agg.avg_qty.val += l_quantity;
                        agg.avg_qty.count++;
                        agg.avg_price.val += l_extendedprice;
                        agg.avg_price.count++;
                        agg.avg_disc.val += l_discount;
                        agg.avg_disc.count++;
                        agg.count_order++;
                    }
                }
            }
        }
        benchmark::DoNotOptimize(groups);
        std::vector<group_key> sorted_groups(groups.size());
        std::transform(groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });

        // pairs in std::pair are compared lexicographically by default,
        // by pair 1 first and pair 2 second.
        std::sort(sorted_groups.begin(), sorted_groups.end());
        benchmark::DoNotOptimize(sorted_groups);
        // for (auto i : sorted_groups) {
        //     printf("\nl_returnflag: %s, l_linestatus: %s, %d, %f, %f, %f, %d, %f, %f, %d",
        //            i.first.c_str(),
        //            i.second.c_str(),
        //            groups[i].sum_qty,
        //            groups[i].sum_base_price,
        //            groups[i].sum_disc_price,
        //            groups[i].sum_charge,
        //            groups[i].avg_qty.val / groups[i].avg_qty.count,
        //            groups[i].avg_price.val / groups[i].avg_price.count,
        //            groups[i].avg_disc.val / groups[i].avg_disc.count,
        //            groups[i].count_order);
        // }
        customerCursor.reset();
    }
    CLEANUP();
}

BENCHMARK(BM_Q1_S2_TPCH)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_Q1_S2_BINARY_TPCH)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_Q1_S1_TPCH)->Unit(benchmark::kMillisecond);

void BM_Q1_S2_BINARY_TPCH(benchmark::State& state) {
    using group_key = BSONObj;
    SETUP();
    Collection* lineitem =
        CollectionCatalog::get(opCtx.get())
            .lookupCollectionByNamespace(opCtx.get(), NamespaceString("tpch_1", "lineitem"));

    for (auto _ : state) {
        struct Aggs {
            int sum_qty;
            double sum_base_price;
            double sum_disc_price;
            double sum_charge;
            AVG<int, int> avg_qty;
            AVG<double, int> avg_price;
            AVG<double, int> avg_disc;
            int count_order;
        };
        auto lineitemCursor = lineitem->getCursor(opCtx.get());
        auto groups = SimpleBSONObjComparator::kInstance.makeBSONObjIndexedUnorderedMap<Aggs>();
        Date_t param1 = dateFromISOString(StringData("1992-01-02T00:00:00Z")).getValue();

        double l_extendedprice, l_discount, l_tax;
        int l_quantity;
        Date_t l_shipdate;
        BSONElement l_returnflag, l_linestatus;
        while (auto record = lineitemCursor->next()) {
            auto bson = record.get().data.toBson();
            try {
                l_shipdate = bson["l_shipdate"].Date();
            } catch (...) {
                continue;
            }
            if (l_shipdate <= param1) {
                try {
                    l_quantity = bson["l_quantity"].Int();
                    l_extendedprice = bson["l_extendedprice"].Number();
                    l_discount = bson["l_discount"].Number();
                    l_tax = bson["l_tax"].Number();
                } catch (...) {
                    continue;
                }

                l_returnflag = bson["l_returnflag"];
                l_linestatus = bson["l_linestatus"];
                BSONObjBuilder b(l_returnflag.size() + l_linestatus.size());
                b.append(l_returnflag);
                b.append(l_linestatus);
                group_key group = b.obj();

                Aggs& agg = groups[group];
                agg.sum_qty += l_quantity;
                agg.sum_base_price += l_extendedprice;
                double disc_price = l_extendedprice * (1 - l_discount);
                agg.sum_disc_price += disc_price;
                agg.sum_charge += disc_price * (1 + l_tax);
                agg.avg_qty.val += l_quantity;
                agg.avg_qty.count++;
                agg.avg_price.val += l_extendedprice;
                agg.avg_price.count++;
                agg.avg_disc.val += l_discount;
                agg.avg_disc.count++;
                agg.count_order++;
            }
        }
        benchmark::DoNotOptimize(groups);
        std::vector<group_key> sorted_groups(groups.size());
        std::transform(groups.begin(), groups.end(), sorted_groups.begin(), [](auto pair) {
            return pair.first;
        });

        std::sort(sorted_groups.begin(),
                  sorted_groups.end(),
                  SimpleBSONObjComparator::kInstance.makeLessThan());
        benchmark::DoNotOptimize(sorted_groups);
        // for (auto i : sorted_groups) {
        //     printf("\nl_returnflag: %s, l_linestatus: %s, %d, %f, %f, %f, %d, %f, %f, %d",
        //            i["l_returnflag"].String().c_str(),
        //            i["l_linestatus"].String().c_str(),
        //            groups[i].sum_qty,
        //            groups[i].sum_base_price,
        //            groups[i].sum_disc_price,
        //            groups[i].sum_charge,
        //            groups[i].avg_qty.val / groups[i].avg_qty.count,
        //            groups[i].avg_price.val / groups[i].avg_price.count,
        //            groups[i].avg_disc.val / groups[i].avg_disc.count,
        //            groups[i].count_order);
        // }
        lineitemCursor.reset();
    }
    CLEANUP();
}
BENCHMARK(BM_Q1_S2_BINARY_TPCH);

}  // namespace
}  // namespace tpch
}  // namespace mongo
